using System.Collections.Generic;
using com.ats.navigation.Pooling;
using com.ats.navigation.surface;
using UnityEngine;

namespace com.ats.navigation.ObjectOriented
{
    public class ObjectiveNavigationSystem : INavigationSystem
    {
        private ISurface _surface;
        private readonly SpatialCollection<IUnit> _spatialCollection = new SpatialCollection<IUnit>();
        private CrowdDensity _crowdDensity = null;
        private IList<UnitNavigation> _unitNavigations = null;
        private IList<IUnit> _units = null;
        private int _numUnits = 0;

        public IList<IUnit> Units => _units;

        public void Dispose()
        {
            
        }

        public void Initialize()
        {
            _crowdDensity = new CrowdDensity(_surface);
            _unitNavigations = Pool<PoolableList<UnitNavigation>>.Get();
            _units = Pool<PoolableList<IUnit>>.Get();
        }

        public void Update()
        {
            _spatialCollection.Build(_spatialCollection.root,_units);
            _crowdDensity.UpdateCrowdDensity(_spatialCollection);
            for (var i = 0; i < _numUnits; i++)
            {
                var unit = _units[i];
                var unitNavigation = _unitNavigations[i];
                var group = _spatialCollection.GetNearestGroup(unit.Position);
                unitNavigation.Update(group, Time.deltaTime, Time.frameCount);
            }
        }

        public void AddUnit(IUnit unit)
        {
            unit.Index = GetNewUniqueIndex();
            ++_numUnits;
            var unitNavigation = new UnitNavigation(unit,_crowdDensity,_surface);
            _unitNavigations.Add(unitNavigation);
            _units.Add(unit);
        }

        public void RemoveUnit(IUnit unit)
        {
            for (int i = _numUnits; i >= 0; i--)
            {
                var unitNavigation = _unitNavigations[i];
                if (unitNavigation.Unit != unit)
                {
                    continue;
                }
                _unitNavigations.RemoveAt(i);
                _units.RemoveAt(i);
                return;
            }
            _units.Remove(unit);
            --_numUnits;
        }

        public void Move(IUnit unit, Vector3 target)
        {
            for (int i = 0; i < _numUnits; i++)
            {
                var unitNavigation = _unitNavigations[i];
                if (unitNavigation.Unit != unit)
                {
                    continue;
                }
                unitNavigation.MoveTo(target);
                return;
            }
        }

        public void SetSurface(ISurface surface)
        {
            _surface = surface;
        }

        private static int _uniqueIndex = 0;

        private static int GetNewUniqueIndex()
        {
            return _uniqueIndex++;
        }
    }
}