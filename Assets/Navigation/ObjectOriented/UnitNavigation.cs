﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.ats.navigation.surface;
using UnityEngine;
using Random = UnityEngine.Random;

namespace com.ats.navigation.ObjectOriented
{
    public class UnitNavigation
    {
        private readonly IUnit _unit;
        private readonly ISurface _surface;
        private readonly PathFinder _pathFinder;
        private readonly Boids<IUnit> _boidSystem = new Boids<IUnit>();
        private readonly IGraphWeights _graphWeightsFromPositions = null;
        
        private Vector3 _averageGroupSpeed;
        private Vector3 _targetPosition;
        private int _targetIdx = -1;

        private const float Speed = 1f;
        private const float RotationSpeedAngles = 360f;

        public IUnit Unit => _unit;

        public void MoveTo(Vector3 targetPosition)
        {
            _targetIdx = _surface.GetNearestNode(targetPosition).index;
            _targetPosition = targetPosition;
        }

        public UnitNavigation(IUnit unit, IGraphWeights graphWeights, ISurface surface)
        {
            _unit = unit;
            _surface = surface;
            _pathFinder = new PathFinder(Heuristic, Distance, Neighbours, Weight,LineOfSight);
            _graphWeightsFromPositions = graphWeights;
        }

        public void Update(IList<IUnit> boids, float timestep, int tick)
        {
            _averageGroupSpeed = Vector3.zero;
            var numBoids = boids.Count;

            for (int i = 0; i < numBoids; ++i)
            {
                _averageGroupSpeed += boids[i].Velocity;
            }


            Vector3 desiredVelocity = _unit.Velocity;
            var collisionCheck = _boidSystem.CheckCollision(_unit, boids);

            var navigationVelocity = GetVelocityFromNavigation();
            var avoidanceData =
                _boidSystem.GetAvoidanceVector(_unit.Index, _unit.Position, navigationVelocity, boids,
                    timestep);
            if (collisionCheck.result && avoidanceData.nearestNeighbourIdx > _unit.Index)
            {
                var nearest = _surface.GetNearestNode(_unit.Position);
                //todo ot: remove LINQ
                var selectedNeighbour = nearest.neighbours.OrderBy(neighbour =>
                    Vector3.Distance(_surface.Graph[neighbour].position,
                        _unit.Position + Speed * timestep * avoidanceData.vector.normalized)).First();

                avoidanceData.vector = (_surface.Graph[selectedNeighbour].position - _unit.Position).normalized;

                desiredVelocity = avoidanceData.vector;
            }
            else
            {
                desiredVelocity = navigationVelocity;
            }

            //todo ot: check collision avoidance, add interface for pooling to easier switch impl 
            _unit.Velocity = Vector3.RotateTowards(_unit.Velocity, desiredVelocity,
                Mathf.Deg2Rad * RotationSpeedAngles * timestep,float.MaxValue).normalized * Speed;
            _unit.Position += _unit.Velocity * timestep;
        }

        private Vector3 GetVelocityFromNavigation()
        {
            if (_targetIdx == -1)
            {
                return Vector3.zero;
            }

            var position = _unit.Position;
            var nearestNode = _surface.GetNearestNode(position);
            var path = _pathFinder.FindPathTheta(nearestNode.index, _targetIdx);

            if (Vector3.Distance(position,_targetPosition) <= _unit.Radius)
            {
                return Vector3.zero;
            }

            if (path == null)
            {
                return Vector3.MoveTowards(position, _targetPosition, Speed * Time.deltaTime) - position;
            }
            
            var pathCount = path.Count;
            if (pathCount <= 1) 
            {
                return Vector3.MoveTowards(position, _targetPosition, Speed * Time.deltaTime) - position;
            }

            int nextPathIndex = 0;
            var nextTargetPosition = _surface.Graph[path[nextPathIndex]].position;
            while (Vector3.Distance(position, nextTargetPosition) < _unit.Radius || nextPathIndex >= _surface.Graph.Count)
            {
                nextTargetPosition = _surface.Graph[path[nextPathIndex]].position;
                ++nextPathIndex;
            }
            return Vector3.MoveTowards(position, nextTargetPosition, Speed * Time.deltaTime) - position;
        }
        
        private int Weight(int index)
        {
            return _surface.Graph[index].weight;
        }


        private bool LineOfSight(int from, int to)
        {
            return _surface.VisibilityGraph.Get(from, to);
        }

        private List<int> Neighbours(int index)
        {
            return _surface.Graph[index].neighbours;
        }

        private int Distance(int node1, int node2)
        {
            return Mathf.Max(1, (int) Vector3.Distance(
                _surface.Graph[node1].position,
                _surface.Graph[node2].position
            ));
        }

        public float GetNavigationDistance(Vector3 from, Vector3 to)
        {
            var fromIdx = _surface.GetNearestNode(from);
            var toIdx = _surface.GetNearestNode(to);
            var path = _pathFinder.FindPathTheta(fromIdx.index, toIdx.index);

            if (path.Count < 2)
            {
                return Vector3.Distance(from, to);
            }

            float totalDistance = Vector3.Distance(from,_surface.Graph[path[0]].position);
            for (var i = 0; i < path.Count-1; i++)
            {
                var node = _surface.Graph[i];
                var nextNode = _surface.Graph[i + 1];
                totalDistance += Vector3.Distance(node.position, nextNode.position);
            }

            totalDistance += Vector3.Distance(_surface.Graph[path[path.Count - 1]].position, to);
            return totalDistance;
        }

        private int Heuristic(int node)
        {
            var targetPos = _surface.Graph[_targetIdx].position;
            var currentPos = _surface.Graph[node].position;
            var dir = targetPos - currentPos;
            var distance = (int) dir.magnitude;
            var crowdPositionWeight = _graphWeightsFromPositions.GetWeight(_surface.Graph[node]);
            var speedWeightRatio = Vector3.Dot(dir.normalized, _averageGroupSpeed.normalized);
            return (int) (distance + 1000 * crowdPositionWeight * (1f - speedWeightRatio));
        }
        //
        // public Vector3 Position => transform.position;
        // public Vector3 Velocity => _currentVelocity;
        // public float Radius => radius;
    }
}