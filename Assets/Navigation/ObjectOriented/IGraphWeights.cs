using com.ats.navigation.surface;

namespace com.ats.navigation.ObjectOriented
{
    public interface IGraphWeights
    {
        int GetWeight(Node node);
    }
}