﻿using System;
using System.Collections.Generic;
using com.ats.navigation.Pooling;
using UnityEngine;

namespace com.ats.navigation.ObjectOriented
{
    public class SpatialCollection<T> where T : IUnit
    {
        public int groupSize = 8;
        private int _maxTreeDepth = 3;
        private Queue<Node> _queue = new Queue<Node>();

        public Node root = new Node();

        public IList<T> GetNearestGroup(Vector3 position)
        {
            Node currentNode = root;
            while (currentNode.elements == null)
            {
                float minDistance = float.MaxValue;
                int minIdx = -1;
                for (var i = 0; i < currentNode.children.Length; i++)
                {
                    var child = currentNode.children[i];
                    if (child == null)
                    {
                        continue;
                    }

                    var distance = Vector3.SqrMagnitude(position - child.center);
                    if (distance < minDistance)
                    {
                        minIdx = i;
                        minDistance = distance;
                    }
                }

                currentNode = currentNode.children[minIdx];
            }

            return currentNode.elements;
        }

        public void Build(Node node, IList<T> elements)
        {
            int numElements = elements.Count;
            int numGroups = Mathf.CeilToInt(numElements / (float)groupSize);
            _maxTreeDepth = Mathf.CeilToInt(Mathf.Log(numGroups, 4));

            Build(node, elements, 0);
        }

        private void Build(Node node, IList<T> elements, int depth)
        {
            int numElements = elements.Count;
            node.Dispose();

            var averagePos = Vector3.zero;
            for (int i = 0; i < numElements; i++)
            {
                averagePos += elements[i].Position;
            }

            averagePos /= numElements;

            using (var _minXminY = Pool<PoolableList<T>>.Get())
            {
                using (var _maxXminY = Pool<PoolableList<T>>.Get())
                {
                    using (var _maxXmaxY = Pool<PoolableList<T>>.Get())
                    {
                        using (var _minXmaxY = Pool<PoolableList<T>>.Get())
                        {
                            node.center = averagePos;

                            if (numElements <= groupSize || depth >= _maxTreeDepth)
                            {
                                if (node.elements == null)
                                {
                                    node.elements = Pool<PoolableList<T>>.Get();
                                }

                                for (var i = 0; i < numElements; i++)
                                {
                                    node.elements.Add(elements[i]);
                                }

                                return;
                            }

                            if (numElements == 0)
                            {
                                return;
                            }

                            for (int i = 0; i < numElements; i++)
                            {
                                var element = elements[i];
                                var position = element.Position;

                                if (position.x <= averagePos.x && position.z <= averagePos.z)
                                {
                                    _minXminY.Add(element);
                                }
                                else if (position.x > averagePos.x && position.z <= averagePos.z)
                                {
                                    _maxXminY.Add(element);
                                }
                                else if (position.x > averagePos.x && position.z > averagePos.z)
                                {
                                    _maxXmaxY.Add(element);
                                }
                                else
                                {
                                    _minXmaxY.Add(element);
                                }
                            }


                            for (int i = 0; i < 4; ++i)
                            {
                                if (node.children[i] == null)
                                {
                                    node.children[i] = Pool<Node>.Get();
                                }
                                else
                                {
                                    node.children[i].Dispose();
                                }
                            }

                            if (_minXminY.Count > 0) Build(node.children[0], _minXminY, depth + 1);
                            else node.children[0] = null;
                            if (_maxXminY.Count > 0) Build(node.children[1], _maxXminY, depth + 1);
                            else node.children[1] = null;
                            if (_maxXmaxY.Count > 0) Build(node.children[2], _maxXmaxY, depth + 1);
                            else node.children[2] = null;
                            if (_minXmaxY.Count > 0) Build(node.children[3], _minXmaxY, depth + 1);
                            else node.children[3] = null;
                        }
                    }
                }
            }
        }

        public void ForEach(Action<Node> action)
        {
            _queue.Clear();
            _queue.Enqueue(root);

            while (_queue.Count > 0)
            {
                var current = _queue.Dequeue();
                if (current == null)
                {
                    continue;
                }

                action(current);
                if (current.children == null)
                {
                    continue;
                }

                for (var index = 0; index < current.children.Length; index++)
                {
                    var currentChild = current.children[index];
                    _queue.Enqueue(currentChild);
                }
            }
        }

        public class Node : IPoolable
        {
            public Vector3 center;
            public Node[] children = new Node[4];
            public PoolableList<T> elements = null;

            public void Dispose()
            {
                elements?.Dispose();
                elements = null;
            }

            public bool IsPooled { get; set; }
        }
    }
}