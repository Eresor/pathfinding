using System.Collections.Generic;
using com.ats.navigation.surface;
using UnityEngine;

namespace com.ats.navigation.ObjectOriented
{
    public class CrowdDensity : IGraphWeights
    {
        private ISurface _surface;
        private Dictionary<Node, int> _weights = new Dictionary<Node, int>();

        public CrowdDensity(ISurface surface)
        {
            _surface = surface;
        }

        public void UpdateCrowdDensity<T>(SpatialCollection<T> collection) where T : IUnit
        {
            for (var i = 0; i < _surface.Graph.Count; i++)
            {
                var node = _surface.Graph[i];
                _weights[node] = 0;
            }

            collection.ForEach(UpdateNodeAction);
        }

        private void UpdateNodeAction<T>(SpatialCollection<T>.Node spatialNode) where T : IUnit
        {
            var nearestGridNode = _surface.GetNearestNode(spatialNode.center);
            _weights[nearestGridNode] += spatialNode.elements?.Count ?? 0;
        }

        public int GetWeight(Node node)
        {
            return _weights[node];
        }
    }
}