﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.ats.navigation.ObjectOriented
{
    public class PathFinder
    {

        private readonly Func<int, int> _heuristic;
        private readonly Func<int, int, int> _distance;
        private readonly Func<int, List<int>> _neighbours;
        private readonly Func<int, int> _weight;
        private readonly Func<int, int, bool> _lineOfSigh;

        private readonly NodeCollection openSet = new NodeCollection();
        private readonly HashSet<int> closedSet = new HashSet<int>();
        
        private Dictionary<int,int> hScore = new Dictionary<int, int>();
        private Dictionary<int,int> gScore = new Dictionary<int, int>();
        private Dictionary<int,int> cameFromIdx = new Dictionary<int, int>();
        
        private readonly List<int> _finalPath = new List<int>();

        public PathFinder(Func<int, int> heuristic,
            Func<int, int, int> distance,
            Func<int, List<int>> neighbours,
            Func<int,int> weight,
            Func<int,int,bool> lineOfSigh = null)
        {
            _heuristic = heuristic;
            _distance = distance;
            _neighbours = neighbours;
            _weight = weight;
            _lineOfSigh = lineOfSigh;
        }

        public List<int> FindPath(int startIdx, int endIdx)
        {
            hScore.Clear();
            gScore.Clear();
            cameFromIdx.Clear();
            closedSet.Clear();
            openSet.Clear();
            openSet.Add(startIdx,0);
            gScore[startIdx] = 0;

            while (openSet.Empty() == false)
            {
                var currentIdx = openSet.Min();
                if (currentIdx == endIdx)
                {
                    return ReconstructPath(startIdx,endIdx);
                }
                
                openSet.Remove(currentIdx);
                closedSet.Add(currentIdx);

                var list = GetNeighbours(currentIdx);
                for (var index = 0; index < list.Count; index++)
                {
                    var neighbourIdx = list[index];
                    if (closedSet.Contains(neighbourIdx))
                    {
                        continue;
                    }

                    var tentativeGScore = gScore[currentIdx] + GetDistance(currentIdx, neighbourIdx)
                        + _weight(neighbourIdx);
                    bool tentativeIsBetter = false;
                    var tentativeHScore = GetHeuristicValue(neighbourIdx);
                    if (openSet.Add(neighbourIdx,tentativeGScore+tentativeHScore))
                    {
                        hScore[neighbourIdx] = tentativeHScore;
                        tentativeIsBetter = true;
                    }
                    else if (tentativeGScore < gScore[neighbourIdx])
                    {
                        tentativeIsBetter = true;
                    }

                    if (tentativeIsBetter)
                    {
                        cameFromIdx[neighbourIdx] = currentIdx;
                        gScore[neighbourIdx] = tentativeGScore;
                    }
                }
            }

            return null;
        }
        
        public List<int> FindPathTheta(int startIdx, int endIdx)
        {
            hScore.Clear();
            gScore.Clear();
            cameFromIdx.Clear();
            closedSet.Clear();
            openSet.Clear();
            gScore[startIdx] = 0;
            openSet.Add(startIdx,gScore[startIdx] + GetHeuristicValue(startIdx));
            cameFromIdx[startIdx] = startIdx;

            while (openSet.Empty() == false)
            {
                var currentIdx = openSet.Min();
                if (currentIdx == endIdx)
                {
                    return ReconstructPath(startIdx,endIdx);
                }
                
                openSet.Remove(currentIdx);
                closedSet.Add(currentIdx);
                var list = GetNeighbours(currentIdx);
                for (var index = 0; index < list.Count; index++)
                {
                    var neighbourIdx = list[index];
                    if (closedSet.Contains(neighbourIdx))
                    {
                        continue;
                    }

                    if (!openSet.Contains(neighbourIdx))
                    {
                        gScore[neighbourIdx] = int.MaxValue;
                        cameFromIdx[neighbourIdx] = -1;
                    }
                    
                    var currentParentIdx = cameFromIdx[currentIdx];
                    if(GetIsLineOfSigh(currentParentIdx,neighbourIdx))
                    {
                        var gScoreParent = gScore[currentParentIdx];
                        var distanceParent = GetDistance(currentParentIdx, neighbourIdx);
                        if (gScoreParent + distanceParent < gScore[neighbourIdx])
                        {
                            gScore[neighbourIdx] = gScoreParent + distanceParent;
                            cameFromIdx[neighbourIdx] = currentParentIdx;
                            if (openSet.Contains(neighbourIdx))
                            {
                                openSet.Remove(neighbourIdx);
                            }
                            openSet.Add(neighbourIdx, gScore[neighbourIdx] + GetHeuristicValue(neighbourIdx));
                        }
                    }
                    else
                    {
                        var currentScore = gScore[currentIdx];
                        var currentDistance = GetDistance(currentIdx, neighbourIdx);
                        if (currentScore + currentDistance < gScore[neighbourIdx])
                        {
                            gScore[neighbourIdx] = currentScore + currentDistance;
                            cameFromIdx[neighbourIdx] = currentIdx;
                            if (openSet.Contains(neighbourIdx))
                            {
                                openSet.Remove(neighbourIdx);
                            }
                            openSet.Add(neighbourIdx, gScore[neighbourIdx] + GetHeuristicValue(neighbourIdx));
                        }
                    }
                    
                }
            }

            return null;
        }

        private bool GetIsLineOfSigh(int from, int to)
        {
            return _lineOfSigh(from, to);
        }

        private int GetHeuristicValue(int node)
        {
            return _heuristic(node);
        }

        private int GetDistance(int current, int neighbour)
        {
            return _distance(current, neighbour);
        }

        private List<int> GetNeighbours(int node)
        {
            return _neighbours(node);
        }

        List<int> ReconstructPath(int startIdx, int endIdx)
        {
            int currentIdx = endIdx;
            _finalPath.Clear();
            while (currentIdx != startIdx)
            {
                _finalPath.Add(currentIdx);
                currentIdx = cameFromIdx[currentIdx];
            }
            _finalPath.Add(currentIdx);

            _finalPath.Reverse(0, _finalPath.Count);
            return _finalPath;
        }
    }

    class NodeCollection
    {
        private readonly Dictionary<int, int> _indexedNodes = new Dictionary<int, int>();
        private readonly List<Node> _sortedNodes = new List<Node>();

        public bool Add(int idx, int score)
        {
            bool alreadyExist = _indexedNodes.ContainsKey(idx);

            if (!alreadyExist)
            {
                _sortedNodes.Add(new Node()
                {
                    Idx = idx,
                    Score = score
                });
                _indexedNodes[idx] = score;
                return true;
            }
            return false;
        }

        public int Min()
        {
            var minValue = float.MaxValue;
            var minIdx = -1;
            for (var i = 0; i < _sortedNodes.Count; i++)
            {
                var node = _sortedNodes[i];
                if (node.Score < minValue)
                {
                    minIdx = node.Idx;
                    minValue = node.Score;
                }
            }
            return minIdx;
        }

        public void Remove(int idx)
        {
            bool alreadyExist = _indexedNodes.ContainsKey(idx);
            if (alreadyExist)
            {
                _indexedNodes.Remove(idx);

                int numElements = _sortedNodes.Count;
                for (int i = numElements - 1; i >= 0; --i)
                {
                    if (_sortedNodes[i].Idx == idx)
                    {
                        _sortedNodes.RemoveAt(i);
                    }
                }
            }
        }

        public void Clear()
        {
            _sortedNodes.Clear();
            _indexedNodes.Clear();
        }

        public bool Empty()
        {
            return _sortedNodes.Count == 0;
        }

        public bool Contains(int idx)
        {
            return _indexedNodes.ContainsKey(idx);
        }

        private struct Node : IComparable<Node>
        {
            public int Idx;
            public int Score;
            
            public int CompareTo(Node other)
            {
                int scoreResult = Score.CompareTo(other.Score);
                return scoreResult == 0 ? Idx.CompareTo(other.Idx) : scoreResult;
            }
        }
    }
}