﻿using System.Collections.Generic;
using com.ats.navigation.Pooling;
using UnityEngine;

namespace com.ats.navigation.ObjectOriented
{
    public class Boids<T> where T : IUnit
    {
        public float angleStep = 1f;
        
//        public AvoidanceData GetAvoidanceVector(int idx, Vector3 position, Vector3 velocity, IList<T> boids, float timestep)
//        {
//            int counter = 0;
//            bool willCollideAny = false;
//            var avoidanceData = new AvoidanceData();
//            do
//            {
//                avoidanceData.vector = Quaternion.AngleAxis(angleStep * counter, Vector3.up) * velocity;
//                var desiredPosition = position + avoidanceData.vector * timestep;
//                var collisionQueryData = CheckCollision(idx,desiredPosition, boids, timestep);
//                willCollideAny = collisionQueryData.result;
//                avoidanceData.distance = collisionQueryData.distance;
//                counter += (counter >= 0 ? 1 : -1);
//                counter *= -1;
//                if (counter > 360 || counter < -360)
//                {
//                    avoidanceData.vector = Quaternion.AngleAxis(Random.Range(-180,180), Vector3.up) * velocity * 0.5f;
//                    return avoidanceData;
//                }
//
//            } while (willCollideAny);
//
//            return avoidanceData;
//        }

        public AvoidanceData GetAvoidanceVector(int idx, Vector3 position, Vector3 velocity, IList<T> boids,
            float timestep)
        {
            var predictedPositions = Pool<PoolableList<Vector3>>.Get();
            var nearestDistance = float.MaxValue;
            var nearestIdx = -1;
            for (var i = 0; i < boids.Count; i++)
            {
                var boid = boids[i];
                if (boid.Index == idx)
                {
                    continue;
                }

                var boidPredictedPos = boid.Position + boid.Velocity * timestep;
                predictedPositions.Add(boidPredictedPos);
                var distance = Vector3.Distance(boidPredictedPos, position) - boid.Radius;
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestIdx = boid.Index;
                }
            }
            
            Vector3 avereage = Vector3.zero;
            for (var i = 0; i < predictedPositions.Count; i++)
            {
                var predictedPosition = predictedPositions[i];
                var directionToPredicted = (predictedPosition - position).normalized;
                avereage += directionToPredicted;
            }
            avereage /= predictedPositions.Count;
            
            predictedPositions.Dispose();
            return new AvoidanceData()
            {
                distance = nearestDistance,
                nearestNeighbourIdx = nearestIdx,
                vector = (-avereage)*velocity.magnitude
            };
        }


        public CollisionQueryData CheckCollision(T boid, IList<T> boids)
        {
            var timestep = boid.Radius / boid.Velocity.magnitude;
            var collisionQueryData = new CollisionQueryData()
            {
                distance = float.MaxValue,
                result = false
            };
            for (var i = 0; i < boids.Count; i++)
            {
                var neighbour = boids[i];
                if (neighbour.Index == boid.Index)
                {
                    continue;
                }
                var predictedNeighbourPosition = neighbour.Position + timestep * neighbour.Velocity;
                var distance = Vector3.Distance(boid.Position + boid.Velocity * timestep, predictedNeighbourPosition);
                collisionQueryData.distance = Mathf.Min(collisionQueryData.distance, distance);
                if(distance < neighbour.Radius)
                {
                    collisionQueryData.result = true;
                    break;
                }
            }

            return collisionQueryData;
        }
    }

    public struct CollisionQueryData
    {
        public bool result;
        public float distance;
    }

    public struct AvoidanceData
    {
        public Vector3 vector;
        public float distance;
        public float nearestNeighbourIdx;
    }
}