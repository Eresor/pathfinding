﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace com.ats.navigation.Tests
{
    [Obsolete]
    public class TestGraphNodeView : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler
    {
        [SerializeField] private Text label = null;
        [SerializeField] private Image image = null;
        public bool isLocked = false;
        public int index;
        
        public void SetData(int index)
        {
            isLocked = false;
            this.index = index;
            label.text = index.ToString();
            SetIsPathNode(false);
        }

        private void SetIsLocked(bool locked)
        {
            isLocked = locked;
            label.text = isLocked ? string.Empty : index.ToString();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (Mouse.current.leftButton.wasReleasedThisFrame)
            {
                SetIsLocked(true);
            }
            if (Mouse.current.rightButton.wasReleasedThisFrame)
            {
                SetIsLocked(false);
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Mouse.current.leftButton.wasReleasedThisFrame)
            {
                SetIsLocked(true);
            }

            if (Mouse.current.rightButton.wasReleasedThisFrame)
            {
                SetIsLocked(false);
            }
        }

        public void SetIsPathNode(bool isPathNode)
        {
            image.color = isPathNode ? Color.green : Color.black;
        }
    }
}