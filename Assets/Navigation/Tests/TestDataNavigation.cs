﻿using System;
using System.Linq;
using com.ats.navigation.DataOriented;
using com.ats.navigation.surface.Data;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Jobs;

namespace com.ats.navigation.Tests
{
    public class TestDataNavigation : TestNavigationBase
    {
        private IDisposable _moveCommand;
        private TransformAccessArray _unitsTransformAccess;

        private DataNavigationSystem _dataNavigation;

        private void OnDestroy()
        {
            _dataNavigation.Dispose();
            _unitsTransformAccess.Dispose();
        }
        
        protected override void Initialize()
        {
            _dataNavigation = new DataNavigationSystem();
            _dataNavigation.Initialize();
            _dataNavigation.SetSurface(Surface);
            
            _unitsTransformAccess = new TransformAccessArray(Units.Select(u => u.transform).ToArray());
            
            for (var i = 0; i < Units.Count; i++)
            {
                var unit = Units[i];
                unit.Position = unit.transform.position;
                _dataNavigation.AddUnit(unit);
            }
        }

        protected override void UpdateNavigation()
        {
            _dataNavigation.Update();
            var moveUnitJob = new DataMovementSystemJob(_dataNavigation.UnitsData);
            var moveUnitJobHandle = moveUnitJob.Schedule(_unitsTransformAccess);
            moveUnitJobHandle.Complete();
        }


        public override void SetTarget(Vector3 mouseHitPosition)
        {
            var target = Surface.GetNearestNode(mouseHitPosition);
            var targetPosition = target.position;
            
            for (var i = 0; i < Units.Count; i++)
            {
                _dataNavigation.Move(Units[i],targetPosition);
            }
        }
    }
}