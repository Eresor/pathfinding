using System;
using System.Collections.Generic;
using com.ats.navigation.ObjectOriented;
using com.ats.navigation.surface;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.ats.navigation.Tests
{
    public class GraphDataVisualization : MonoBehaviour
    {
        [NonSerialized] public IGraphWeights graphWeights;
        [NonSerialized] public IReadOnlyList<Node> graph;

        public void SetData(IReadOnlyList<Node> graph, IGraphWeights graphWeights)
        {
            this.graphWeights = graphWeights;
            this.graph = graph;
        }
    }

    #if UNITY_EDITOR
    [CustomEditor(typeof(GraphDataVisualization))]
    public class GraphDataVisualizationEditor : Editor
    {
        private void OnSceneGUI()
        {
            var dataComponent = target as GraphDataVisualization;

            var textStyle = new GUIStyle();
            textStyle.normal.textColor = Color.black;
            Vector3[] line = new Vector3[2];
            if (dataComponent.graph == null || dataComponent.graphWeights == null)
            {
                return;
            }
            for (var i = 0; i < dataComponent.graph.Count; i++)
            {
                var node = dataComponent.graph[i];
                line[0] = node.position;

                string label = dataComponent.graphWeights.GetWeight(node).ToString();

                Handles.Label(node.position + 0.2f * Vector3.up, label, textStyle);
                foreach (var neighbour in node.neighbours)
                {
                    line[1] = dataComponent.graph[neighbour].position;
                    Handles.DrawPolyLine(line);
                }
            }
        }
    }
    #endif
}