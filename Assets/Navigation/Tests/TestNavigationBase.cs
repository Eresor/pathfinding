using System;
using System.Collections.Generic;
using System.Linq;
using com.ats.navigation.DataOriented;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using UnityEngine;
using UnityEngine.InputSystem;

namespace com.ats.navigation.Tests
{
    public abstract class TestNavigationBase : MonoBehaviour
    {
        [SerializeField] private bool is2d = false;
        [SerializeField] private Transform targetIndicator;
        [SerializeField] private surface.Surface surface;
        [SerializeField] private List<UnitComponent> units;
        private RaycastHit[] _raycastResults = new RaycastHit[100];
        private RaycastHit2D[] _raycastResults2d = new RaycastHit2D[100];

        protected List<UnitComponent> Units => units;
        protected surface.Surface Surface => surface;

        public void Awake()
        {
            Initialize();
        }

        private void Update()
        {
            if (Mouse.current.leftButton.wasReleasedThisFrame)
            {
                OnClick();
            }

            UpdateNavigation();
        }
        
        private void OnClick()
        {
            var cameraOriginRay = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            var direction = cameraOriginRay.direction;
            var origin = cameraOriginRay.origin;
            int numResults = 0;
            if (is2d)
            {
                numResults =
                    Physics2D.RaycastNonAlloc(origin, direction, _raycastResults2d);
            }
            else
            {
                numResults =
                    Physics.RaycastNonAlloc(origin,direction, _raycastResults);
            }
                
            if (numResults == 0)
            {
                return;
            }

            var target = is2d ? new Vector3(_raycastResults2d[0].point.x,_raycastResults2d[0].point.y,0) : _raycastResults[0].point;

            targetIndicator.transform.position = target;
            SetTarget(target);
        }

#if ODIN_INSPECTOR
        [ShowInInspector]
#endif
        private void PopuplateUnits()
        {
            units = FindObjectsOfType<UnitComponent>().Where(unit => unit.gameObject.activeInHierarchy).ToList();
        }
        
        protected abstract void Initialize();
        public abstract void SetTarget(Vector3 mouseHitPoint);
        protected abstract void UpdateNavigation();

    }
}