using com.ats.navigation.ObjectOriented;
using UnityEngine;

namespace com.ats.navigation.Tests
{
    public class TestObjectiveNavigation : TestNavigationBase
    {
        private ObjectiveNavigationSystem _navigation;

        protected override void Initialize()
        {
            _navigation = new ObjectiveNavigationSystem();
            _navigation.SetSurface(Surface);
            _navigation.Initialize();
            for (var i = 0; i < Units.Count; i++)
            {
                var unitComponent = Units[i];
                unitComponent.Position = unitComponent.transform.position;
                _navigation.AddUnit(unitComponent);
            }
        }

        public override void SetTarget(Vector3 mouseHitPoint)
        {
            var target = Surface.GetNearestNode(mouseHitPoint);
            var targetPosition = target.position;
            
            for (var i = 0; i < Units.Count; i++)
            {
                _navigation.Move(Units[i],targetPosition);
            }
        }

        protected override void UpdateNavigation()
        {
            for (var i = 0; i < _navigation.Units.Count; i++)
            {
                var navigationUnit = _navigation.Units[i];
                Units[i].transform.position = navigationUnit.Position;
            }
        }
    }
}