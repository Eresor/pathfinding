namespace com.ats.navigation.surface
{
    public interface IVisibilityGraph
    {
        bool Get(int from, int to);
    }
}