using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.ats.navigation.surface
{
    [Serializable]
    public class Node : IComparable<Node>
    {
        public int index;
        public Vector3 position;
        public List<int> neighbours = new List<int>();
        public float nearestEdgeDistance;
        public int weight;


        public int CompareTo(Node other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return index.CompareTo(other.index);
        }
    }
}