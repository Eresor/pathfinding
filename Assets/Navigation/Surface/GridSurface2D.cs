﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using com.ats.navigation.surface;
using UnityEngine;

public class GridSurface2D : Surface, ISurfaceEditor
{
    [SerializeField] protected List<Node> graph;
    [SerializeField] private float distanceBetweenPoints = 1f;
    [SerializeField] private LayerMask solidGeometryLayer;
    public override IReadOnlyList<Node> Graph => graph;
    public bool VisibilityGraphInitialized => visibilityGraph.IsInitialized;
    public bool DrawVisibilityGraphEditor => drawVisibilityGraphEditor;
    public Vector3 SurfaceNormal => surfaceNormal;
    public override void Generate()
    {
        graph.Clear();
        if (radius >= distanceBetweenPoints)
        {
            Debug.LogError("Radius should be smaller than distance between points");
            return;
        }

        var colliders = gameObject.GetComponentsInChildren<Collider2D>();
        var bounds = new Bounds();
        foreach (var collider2D in colliders)
            bounds.Encapsulate(collider2D.bounds);

        var rect = new Rect(bounds.min, bounds.max - bounds.min);
        var raycastOffset = 0.05f * surfaceNormal;

        var queue = new List<Vector3> {Vector2.zero};

        while (queue.Count > 0)
        {
            var currentPoint = queue.First();
            queue.RemoveAt(0);

            var neighbours = GetNeighbourPoints(currentPoint, samplesPerPoint, distanceBetweenPoints);
            foreach (var neighbourPos in neighbours)
            {
                if (!rect.Contains(neighbourPos))
                    continue;

                var nearestEdgeDistance = GetNearestEdgeDistance(neighbourPos, samplesPerPoint);
                if (ValidPoint(neighbourPos, radius, distanceBetweenPoints))// && nearestEdgeDistance >= radius
                {
                    queue.Add(neighbourPos);
                    graph.Add(new Node());
                    int newPointIdx = Graph.Count - 1;
                    Graph[newPointIdx].position = neighbourPos;
                    Graph[newPointIdx].index = newPointIdx;
                    Graph[newPointIdx].nearestEdgeDistance = nearestEdgeDistance;
                }
            }
        }

        for (var i = 0; i < Graph.Count; i++)
        {
            for (int j = 0; j < Graph.Count; j++)
            {
                if (i == j)
                {
                    continue;
                }

                if (Vector3.Distance(Graph[i].position, Graph[j].position) <= 1.1f * distanceBetweenPoints)
                {
                    Graph[i].neighbours.Add(j);
                    Graph[j].neighbours.Add(i);
                }
            }
        }

        for (var i = 0; i < Graph.Count; i++)
        {
            var node = Graph[i];
            node.neighbours = node.neighbours.Distinct().ToList();
        }

        visibilityGraph.GenerateGraph2d(graph, raycastOffset,solidGeometryLayer);
        // OptimizeGraph(raycastOffset);
    }

    private void OptimizeGraph(Vector3 raycastOffset)
    {
        var listToRemove = new List<int>();
        for (var i = Graph.Count - 1; i >= 0; i--)
        {
            var node = Graph[i];
            if (node.neighbours.Count >= 6)
            {
                listToRemove.Add(i);
            }
        }

        for (var index = 0; index < listToRemove.Count; index++)
        {
            graph.RemoveAt(listToRemove[index]);
        }

        for (var i = 0; i < Graph.Count; i++)
        {
            Graph[i].index = i;
            Graph[i].neighbours.Clear();
        }

        for (var i = 0; i < Graph.Count; i++)
        {
            for (int j = 0; j < Graph.Count; j++)
            {
                if (i == j)
                {
                    continue;
                }

                if (Vector3.Distance(Graph[i].position, Graph[j].position) <= 1.1f * distanceBetweenPoints)
                {
                    Graph[i].neighbours.Add(j);
                    Graph[j].neighbours.Add(i);
                }
            }
        }
    }

    public void SetWeight(Vector3 center, float setWeightRadius, int weight)
    {
        var nodes = Graph.Where(node => Vector3.Distance(node.position, center) < setWeightRadius);
        foreach (var node in nodes)
        {
            node.weight = weight;
        }
    }

    public Node GetNearestNode(Vector3 position)
    {
        int numNodes = Graph.Count;
        int minIdx = -1;
        float minDistance = float.MaxValue;
        for (int i = 0; i < numNodes; i++)
        {
            var distance = Vector3.SqrMagnitude(position - Graph[i].position);
            if (distance < minDistance)
            {
                minDistance = distance;
                minIdx = i;
            }
        }

        return Graph[minIdx];
    }

    private bool ValidPoint(Vector3 point, float agentRadius, float distanceBetweenPoints)
    {
        return true;
        Vector3 initialOffset = surfaceForward;
        const int numSamples = 16;
        float angleDiff = 360f / numSamples;
        for (float angle = 0; angle < 360f; angle += angleDiff)
        {
            var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
            var raycastOrigin = point + 0.05f * surfaceNormal;
            bool result = Physics2D.Raycast(raycastOrigin, direction, agentRadius, solidGeometryLayer);
            if (result)
            {
                return false;
            }
        }

        return Graph.All(visitedPoint =>
            Vector3.Distance(visitedPoint.position, point) > 0.5f * distanceBetweenPoints);
    }

    private float GetNearestEdgeDistance(Vector3 center, int samplesPerPoint)
    {
        Vector3 initialOffset = surfaceForward;
        float angleDiff = 360f / samplesPerPoint;
        float nearestEdgeDistance = float.MaxValue;
        for (float angle = 0; angle < 360; angle += angleDiff)
        {
            var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
            var raycastOrigin = center + 0.05f * surfaceNormal;
            var result = Physics2D.Raycast(raycastOrigin, direction, float.MaxValue, solidGeometryLayer);

            if (result.collider == null)
            {
                continue;
            }

            nearestEdgeDistance = Mathf.Min(nearestEdgeDistance, Vector3.Distance(raycastOrigin, result.point));
        }

        return nearestEdgeDistance;
    }

    private List<Vector3> GetNeighbourPoints(Vector3 center, int samplesPerPoint, float distanceBetweenPoints)
    {
        Vector3 initialOffset = surfaceForward;
        float angleDiff = 360f / samplesPerPoint;

        var neighbours = new List<Vector3>();
        for (float angle = 0; angle < 360; angle += angleDiff)
        {
            var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
            var result = Physics2D.Raycast(center + 0.05f * surfaceNormal, direction, distanceBetweenPoints,
                solidGeometryLayer);

            if (result.collider != null)
            {
                continue;
            }

            Vector3 offset = direction * distanceBetweenPoints;
            neighbours.Add(center + offset);
        }

        return neighbours;
    }
}