using UnityEngine;

namespace com.ats.navigation.surface
{
    public interface ISurfaceEditor : ISurface
    {
        void Generate();
        bool VisibilityGraphInitialized { get; }
        bool DrawVisibilityGraphEditor { get; }
        Vector3 SurfaceNormal { get; }
    }
}