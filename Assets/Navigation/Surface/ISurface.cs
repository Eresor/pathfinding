using System.Collections.Generic;
using UnityEngine;

namespace com.ats.navigation.surface
{
    public interface ISurface
    {
        IReadOnlyList<Node> Graph { get; }
        IVisibilityGraph VisibilityGraph { get; }
        Node GetNearestNode(Vector3 position);
        void Generate();
    }
}