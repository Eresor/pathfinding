using System;
using System.Collections.Generic;
using System.Linq;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using UnityEditor;
using UnityEngine;

namespace com.ats.navigation.surface
{
    public class HexSurface : Surface, ISurface, ISurfaceEditor
    {
        [SerializeField] private List<HexNode> graph;
        [SerializeField] public Grid gridComponent;

        public bool VisibilityGraphInitialized => visibilityGraph.IsInitialized;

        public bool DrawVisibilityGraphEditor => drawVisibilityGraphEditor;
        
        public override IReadOnlyList<Node> Graph => graph;
        
        public Vector3 SurfaceNormal => surfaceNormal;
        private Vector3 RaycastOffset => 0.05f * surfaceNormal;

#if ODIN_INSPECTOR
        [ShowInInspector]
#endif
        public override void Generate()
        {
            graph.Clear();
            var queue = new List<Point> {new Point(transform.position, 0, Vector3Int.zero)};
            graph.Add(new HexNode()
            {
                index = 0,
                position = transform.position,
                gridIndex = Vector3Int.zero
            });
            
            while (queue.Count > 0)
            {
                var currentPoint = queue.First();
                queue.RemoveAt(0);
                var neighbours = GetNeighbourPoints(currentPoint.gridIndex)
                    .Where(n=>graph.FirstOrDefault(item=>item.gridIndex==n.gridIdx)==null)
                    .ToList();
                foreach (var pair in neighbours)
                {
                    var neighbourPos = pair.pos;
                    var nearestEdgeDistance = GetNearestEdgeDistance(neighbourPos);
                    graph.Add(new HexNode());
                    int newPointIdx = graph.Count - 1;
                    graph[newPointIdx].position = neighbourPos;
                    graph[newPointIdx].gridIndex = pair.gridIdx;
                    graph[newPointIdx].index = newPointIdx;
                    graph[newPointIdx].nearestEdgeDistance = nearestEdgeDistance;
                    var neighbourPoint = new Point(neighbourPos, newPointIdx,pair.gridIdx);
                    queue.Add(neighbourPoint);
                }
            }

            for (var i = 0; i < graph.Count; i++)
            {
                var node = graph[i];
                node.neighbours = GetNeighbourPoints(node.gridIndex).Select(neighbour=>graph.FirstOrDefault(entry=>entry.gridIndex == neighbour.gridIdx).index).ToList();
            }
            
            visibilityGraph.GenerateGraph(graph.Cast<Node>().ToList(),RaycastOffset);
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        private List<(Vector3 pos,Vector3Int gridIdx)> GetNeighbourPoints(Vector3Int gridIndex)
        {
            var center = gridComponent.CellToWorld(gridIndex);
            return new List<Vector3Int>()
                {
                    new Vector3Int(gridIndex.x - 1, gridIndex.y + 1, 0),
                    new Vector3Int(gridIndex.x, gridIndex.y + 1, 0),
                    
                    new Vector3Int(gridIndex.x - 1, gridIndex.y, 0),
                    new Vector3Int(gridIndex.x + 1, gridIndex.y, 0),
                    
                    new Vector3Int(gridIndex.x - 1, gridIndex.y - 1, 0),
                    new Vector3Int(gridIndex.x, gridIndex.y - 1, 0),
                }.Select(idx => (gridComponent.CellToWorld(idx),idx))
                .Where(pair =>
                {
                    var maxDistance = 1.5f;
                    bool edgeExist = Physics.Raycast(center + 0.05f * surfaceNormal, pair.Item1 - center,
                            Vector3.Distance(pair.Item1, center));
                    
                    var neighbourPos = pair.Item1;
                    bool isHit = Physics.Raycast(neighbourPos + RaycastOffset, -surfaceNormal, 0.1f);
                    var nearestEdgeDistance = GetNearestEdgeDistance(neighbourPos);
                    return !edgeExist && isHit && nearestEdgeDistance >= radius && Vector3.Distance(center,pair.Item1) < maxDistance;
                }).ToList();
        }

        [Serializable]
        public class Point
        {
            public Vector3 position;
            public int index;
            public Vector3Int gridIndex;

            public Point(Vector3 position, int index, Vector3Int gridIndex)
            {
                this.position = position;
                this.index = index;
                this.gridIndex = gridIndex;
            }
        }

        [Serializable]
        public class HexNode : Node
        {
            public Vector3Int gridIndex;
        }

    }
}