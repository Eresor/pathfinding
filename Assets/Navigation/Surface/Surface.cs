using System.Collections.Generic;
using UnityEngine;

namespace com.ats.navigation.surface
{
    public abstract class Surface : MonoBehaviour, ISurface
    {
        [SerializeField] protected VisibilityGraph visibilityGraph;
        [SerializeField] protected Vector3 surfaceNormal = Vector3.up;
        [SerializeField] protected Vector3 surfaceForward = Vector3.forward;
        [SerializeField] protected float radius = 0.5f;
        [SerializeField] protected int samplesPerPoint = 8;
        [SerializeField] protected bool drawVisibilityGraphEditor;

        public abstract IReadOnlyList<Node> Graph { get; }
        public IVisibilityGraph VisibilityGraph => visibilityGraph;

        public Node GetNearestNode(Vector3 position)
        {
            int numNodes = Graph.Count;
            int minIdx = -1;
            float minDistance = float.MaxValue;
            for (int i = 0; i < numNodes; i++)
            {
                var distance = Vector3.SqrMagnitude(position - Graph[i].position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    minIdx = i;
                }
            }
            return Graph[minIdx];
        }

        public abstract void Generate();

        protected float GetNearestEdgeDistance(Vector3 center)
        {
            Vector3 initialOffset = surfaceForward;
            float angleDiff = 360f / samplesPerPoint;
            float nearestEdgeDistance = float.MaxValue;
            for (float angle = 0; angle < 360; angle += angleDiff)
            {
                var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
                var raycastOrigin = center + 0.05f * surfaceNormal;
                bool result = Physics.Raycast(raycastOrigin, direction, out RaycastHit hitInfo);

                if (!result)
                {
                    continue;
                }

                nearestEdgeDistance = Mathf.Min(nearestEdgeDistance, Vector3.Distance(raycastOrigin, hitInfo.point));
            }

            return nearestEdgeDistance;
        }
    }
}