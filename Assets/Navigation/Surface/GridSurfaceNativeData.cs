﻿using System;
using System.Diagnostics.Contracts;
using com.ats.navigation.DataOriented;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace com.ats.navigation.surface.Data
{
    public static class GridSurfaceExtensions
    {
        public static GridSurfaceNativeData GetNativeData(this ISurface gridSurface, Allocator allocator)
        {
            GridSurfaceNativeData nativeData = new GridSurfaceNativeData();
            nativeData.graph = new NativeList<NativeNode>(allocator);
            int numNodes = gridSurface.Graph.Count;
            for (int i = 0; i < numNodes ; ++i)
            {
                var node = gridSurface.Graph[i];
                var nativeNode = new NativeNode()
                {
                    index = node.index,
                    position = node.position,
                    weight = node.weight
                };

                int numNodeNeighbours = node.neighbours.Count;
                if (numNodeNeighbours >= 1) nativeNode.neighbour0 = node.neighbours[0];
                if (numNodeNeighbours >= 2) nativeNode.neighbour1 = node.neighbours[1];
                if (numNodeNeighbours >= 3) nativeNode.neighbour2 = node.neighbours[2];
                if (numNodeNeighbours >= 4) nativeNode.neighbour3 = node.neighbours[3];
                if (numNodeNeighbours >= 5) nativeNode.neighbour4 = node.neighbours[4];
                if (numNodeNeighbours >= 6) nativeNode.neighbour5 = node.neighbours[5];

                nativeData.graph.Add(nativeNode);
            }

            nativeData.visibility = new NativeMatrix<int>(numNodes,numNodes,allocator);
            for (int x = 0; x < numNodes; x++)
            {
                for (int y = 0; y < numNodes; y++)
                {
                    bool result = gridSurface.VisibilityGraph.Get(x, y);
                    nativeData.visibility[x, y] = result ? 1 : 0;
                }
            }
            
            return nativeData;
        }
    }

    public struct GridSurfaceNativeData : IDisposable
    {
        //todo: convert into NativeMatrix
        public NativeList<NativeNode> graph;
        public NativeMatrix<int> visibility;

        [Pure]
        public NativeNode GetNearestNode(float3 position)
        {
            float distance = float.MaxValue;
            int minIdx = -1;
            for (int i = 0; i < graph.Length; ++i)
            {
                var distancesq = math.distancesq(position, graph[i].position);
                if (distancesq < distance)
                {
                    minIdx = i;
                    distance = distancesq;
                }
            }
            return graph[minIdx];
        }
        
        public void Dispose()
        {
            if (graph.IsCreated)
            {
                graph.Dispose();
            }
            visibility.Dispose();
        }
    }

    public struct NativeNode : IComparable<NativeNode>
    {
        public int index;
        public float3 position;
        public int neighbour0;
        public int neighbour1;
        public int neighbour2;
        public int neighbour3;
        public int neighbour4;
        public int neighbour5;
        public int weight;

        public void GetNeighbours(NativeList<int> container)
        {
            if(neighbour0 >= 0)container.Add(neighbour0);
            if(neighbour1 >= 1)container.Add(neighbour1);
            if(neighbour2 >= 2)container.Add(neighbour2);
            if(neighbour3 >= 3)container.Add(neighbour3);
            if(neighbour4 >= 4)container.Add(neighbour4);
            if(neighbour5 >= 5)container.Add(neighbour5);
        }

        public int CompareTo(NativeNode other)
        {
            return index.CompareTo(other.index);
        }
    }
}