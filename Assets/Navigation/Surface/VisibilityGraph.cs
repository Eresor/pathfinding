﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.ats.navigation.surface
{
    [Serializable]
    public class VisibilityGraph : IVisibilityGraph
    {
        [SerializeField] private List<VisibilityGraphEntry> entries = null;

        public bool IsInitialized => entries != null && entries.Count > 0;
        
        public bool Get(int from, int to)
        {
            return entries[from].entry[to];
        }
        
        public void GenerateGraph(IList<Node> graph, Vector3 raycastOffset)
        {
            entries.Clear();
            int numNodes = graph.Count;
            entries = new List<VisibilityGraphEntry>(numNodes);
            for (var i = 0; i < numNodes; i++)
            {
                var node = graph[i];
                var newEntry = new VisibilityGraphEntry();
                for (int j = 0; j < numNodes; ++j)
                {
                    if (i == j)
                    {
                        newEntry.entry.Add(true);
                        continue;
                    }
                    var direction = graph[j].position - graph[i].position;
                    bool isHit = Physics.Raycast(node.position + raycastOffset,direction.normalized, direction.magnitude);
                    newEntry.entry.Add(!isHit);
                }
                entries.Add(newEntry);
            }
        }
        
        public void GenerateGraph2d(IList<Node> graph, Vector3 raycastOffset, LayerMask layerMask)
        {
            entries.Clear();
            int numNodes = graph.Count;
            entries = new List<VisibilityGraphEntry>(numNodes);
            for (var i = 0; i < numNodes; i++)
            {
                var node = graph[i];
                var newEntry = new VisibilityGraphEntry();
                for (int j = 0; j < numNodes; ++j)
                {
                    if (i == j)
                    {
                        newEntry.entry.Add(true);
                        continue;
                    }
                    var direction = graph[j].position - graph[i].position;
                    bool isHit = Physics2D.Raycast(node.position + raycastOffset,direction.normalized, direction.magnitude,layerMask);
                    newEntry.entry.Add(!isHit);
                }
                entries.Add(newEntry);
            }
        }
        
        [Serializable]
        public class VisibilityGraphEntry
        {
            [SerializeField] public List<bool> entry = new List<bool>();
        }
    }
}