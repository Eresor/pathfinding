﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.ats.surface.colliders
{
    public class Collider : MonoBehaviour 
    {
        [SerializeField][HideInInspector] public ColliderMesh mesh;

        public virtual void RebuildMesh()
        {
        }

        public virtual bool IsInside(Vector3 point)
        {
            var center = transform.position;
            for (int i = 0; i < mesh.triangles.Count; i += 3)
            {
                var t0 = transform.TransformPoint(mesh.vertices[mesh.triangles[i]]);
                var t1 = transform.TransformPoint(mesh.vertices[mesh.triangles[i + 1]]);
                var t2 = transform.TransformPoint(mesh.vertices[mesh.triangles[i + 2]]);

                if (PointInTetrahedron(t0, t1, t2, center, point))
                {
                    return true;
                }
            }
            return false;
        }

        bool PointInTetrahedron(Vector3 v1,Vector3  v2,Vector3  v3,Vector3  v4,Vector3  p)
        {
            return SameSide(v1, v2, v3, v4, p) &&
                   SameSide(v2, v3, v4, v1, p) &&
                   SameSide(v3, v4, v1, v2, p) &&
                   SameSide(v4, v1, v2, v3, p);               
        }
        
        bool SameSide(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 p)
        {
            var normal = Vector3.Cross(v2 - v1, v3 - v1);
            var dotV4 = Vector3.Dot(normal, v4 - v1);
            var dotP = Vector3.Dot(normal, p - v1);
            return Math.Sign(dotV4) == Math.Sign(dotP);
        }
    }

    [Serializable]
    public class ColliderMesh
    {
        [SerializeField] public List<Vector3> vertices;
        [SerializeField] public List<int> triangles;
    }
}