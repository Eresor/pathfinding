﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.ats.surface.colliders
{
    public class SphereCollider : Collider
    {
        [SerializeField] private int numSteps;
        [SerializeField] private float radius;

        public override void RebuildMesh()
        {
            float stepAngleDeg = 360f / numSteps;

            List<Vector3> vertices = new List<Vector3>();
            List<int> triangles = new List<int>();

            vertices.Add(new Vector3(0, radius, 0));

            int numLevels = 0;
            for (float xzAngle = stepAngleDeg; xzAngle < 180; xzAngle += stepAngleDeg)
            {
                numLevels++;
                float xzOffset = radius * Mathf.Sin(xzAngle * Mathf.Deg2Rad);
                float yOffset = radius * Mathf.Cos(xzAngle * Mathf.Deg2Rad);
                for (float yAngle = 0; yAngle < 360f; yAngle += stepAngleDeg)
                {
                    vertices.Add(new Vector3(xzOffset * Mathf.Sin(Mathf.Deg2Rad * yAngle), yOffset,
                        xzOffset * Mathf.Cos(Mathf.Deg2Rad * yAngle)));
                }
            }

            vertices.Add(new Vector3(0, -radius, 0));

            for (int i = 1; i < numSteps; ++i)
            {
                triangles.Add(i);
                triangles.Add(i + 1);
                triangles.Add(0);
            }

            triangles.Add(numSteps);
            triangles.Add(1);
            triangles.Add(0);

            for (int i = 1; i < (numLevels - 1) * numSteps + 1; ++i)
            {
                if (i % numSteps == 0)
                {
                    triangles.Add(i);
                    triangles.Add(i + numSteps);
                    triangles.Add(i + 1);

                    triangles.Add(i - numSteps + 1);
                    triangles.Add(i);
                    triangles.Add(i + 1);
                }
                else
                {
                    triangles.Add(i);
                    triangles.Add(i + numSteps);
                    triangles.Add(i + numSteps + 1);

                    triangles.Add(i + 1);
                    triangles.Add(i);
                    triangles.Add(i + numSteps + 1);
                }
            }

            for (int i = vertices.Count - numSteps - 1; i < vertices.Count - 2; ++i)
            {
                triangles.Add(vertices.Count - 1);
                triangles.Add(i + 1);
                triangles.Add(i);
            }

            triangles.Add(vertices.Count - 1);
            triangles.Add(vertices.Count - numSteps - 1);
            triangles.Add(vertices.Count - 2);

            mesh.vertices = vertices;
            mesh.triangles = triangles;
        }
    }
}
