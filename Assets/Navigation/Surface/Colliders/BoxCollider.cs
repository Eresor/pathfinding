﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace com.ats.surface.colliders
{
    public class BoxCollider : Collider
    {
        [SerializeField] private Vector3 center;
        [SerializeField] private Vector3 size;

        public override void RebuildMesh()
        {
            mesh.vertices = new List<Vector3>();
            mesh.vertices.Add(center + new Vector3(size.x,size.y,size.z)/2);
            mesh.vertices.Add(center + new Vector3(-size.x,size.y,size.z)/2);
            mesh.vertices.Add(center + new Vector3(-size.x,-size.y,size.z)/2);
            mesh.vertices.Add(center + new Vector3(size.x,-size.y,size.z)/2);
            mesh.vertices.Add(center + new Vector3(size.x,size.y,-size.z)/2);
            mesh.vertices.Add(center + new Vector3(-size.x,size.y,-size.z)/2);
            mesh.vertices.Add(center + new Vector3(-size.x,-size.y,-size.z)/2);
            mesh.vertices.Add(center + new Vector3(size.x,-size.y,-size.z)/2);

            mesh.triangles = new List<int>();
            mesh.triangles.AddRange(new []{1,3,0,1,2,3,0,3,4,4,3,7,5,4,7,5,7,6,2,1,5,6,2,5,1,0,4,1,4,5,6,7,2,7,3,2});
        }

        public override bool IsInside(Vector3 point)
        {
            var p1 = transform.TransformPoint(center + size);
            var p2 = transform.TransformPoint(center - size);
            
            var pMax = new Vector3(Mathf.Max(p1.x,p2.x),Mathf.Max(p1.y,p2.y),Mathf.Max(p1.z,p2.z));
            var pMin = new Vector3(Mathf.Min(p1.x,p2.x),Mathf.Min(p1.y,p2.y),Mathf.Min(p1.z,p2.z));

            return point.x <= pMax.x && point.x >= pMin.x && point.y <= pMax.y && point.y >= pMin.y &&
                   point.z <= pMax.z && point.z >= pMin.z;
        }
    }
}