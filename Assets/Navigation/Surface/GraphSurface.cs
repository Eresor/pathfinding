﻿using System.Collections;
using System.Collections.Generic;
using com.ats.navigation.surface;
using UnityEngine;

public class GraphSurface : Surface
{
    [SerializeField] private int numRays = 16;
    [SerializeField] private float maxCastingError = 0.25f;
    public override IReadOnlyList<Node> Graph { get; }
    
    public override void Generate()
    {
        var startPoint = transform.position;
        
    }

    private List<Vector3> GetNeighbourPointsToCheck(Vector3 position)
    {
        var angleDiffDeg = 360f / numRays;
        float rayLength = Mathf.Sin(Mathf.Deg2Rad * angleDiffDeg) * maxCastingError;
        var neighbours = new List<Vector3>(numRays);
        for (float angle = 0; angle < 360f; angle += angleDiffDeg)
        {
            var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * surfaceForward).normalized;
            neighbours.Add(position + direction);
        }
        return neighbours;
    }
}
