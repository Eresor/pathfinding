﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class BakeSurfaceGraph : MonoBehaviour
{
    [SerializeField] private GameObject parent;

    [SerializeField] private List<Vector2> geometry;
    
    #if ODIN_INSPECTOR
    [ShowInInspector]
#endif
    private void SimplifyGeometry()
    {
        var allGeometry = parent.GetComponentsInChildren<Collider>();
        
    }
}
