﻿using System;
using System.Collections.Generic;
using System.Linq;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.ats.navigation.surface
{
    public class GridSurface : Surface, ISurfaceEditor
    {
        [SerializeField] protected List<Node> graph;
        [SerializeField] private float distanceBetweenPoints = 1f;    

        public override IReadOnlyList<Node> Graph => graph;
        public Vector3 SurfaceNormal => surfaceNormal;

        public bool VisibilityGraphInitialized => visibilityGraph.IsInitialized;
        public bool DrawVisibilityGraphEditor => drawVisibilityGraphEditor;
        
#if ODIN_INSPECTOR
        [ShowInInspector]
#endif
        public void Clear()
        {
            graph.Clear();
        }

#if ODIN_INSPECTOR
        [ShowInInspector]
#endif
        public override void Generate()
        {
            var raycastOffset = 0.05f * surfaceNormal;
            graph.Clear();
            var queue = new List<Point> {new Point(transform.position, 0)};
            graph.Add(new Node()
            {
                index = 0,
                position = transform.position
            });
    
            while (queue.Count > 0)
            {
                var currentPoint = queue.First();
                queue.RemoveAt(0);

                var neighbours = GetNeighbourPoints(currentPoint.position);
                foreach (var neighbourPos in neighbours)
                {
                    bool isHit = Physics.Raycast(neighbourPos + raycastOffset, -surfaceNormal, 0.1f);
                    var nearestEdgeDistance = GetNearestEdgeDistance(neighbourPos);
                    if (isHit && ValidPoint(neighbourPos) && nearestEdgeDistance >= radius)
                    {
                        graph.Add(new Node());
                        int newPointIdx = graph.Count - 1;
                        graph[newPointIdx].position = neighbourPos;
                        graph[newPointIdx].index = newPointIdx;
                        graph[newPointIdx].nearestEdgeDistance = nearestEdgeDistance;
                        var neighbourPoint = new Point(neighbourPos, newPointIdx);
                        queue.Add(neighbourPoint);
                    }
                }
            }

            for (var i = 0; i < graph.Count; i++)
            {
                for (int j = 0; j < graph.Count; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    if (Vector3.Distance(graph[i].position, graph[j].position) <= 1.1f*distanceBetweenPoints)
                    {
                        graph[i].neighbours.Add(j);
                        graph[j].neighbours.Add(i);
                    }
                }
            }

            for (var i = 0; i < graph.Count; i++)
            {
                var node = graph[i];
                node.neighbours = node.neighbours.Distinct().ToList();
            }
            
//            visibilityGraph.GenerateGraph(graph,raycastOffset);

//            OptimizeGraph(raycastOffset);
            visibilityGraph.GenerateGraph(graph,raycastOffset);
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        private void OptimizeGraph(Vector3 raycastOffset)
        {
            var listToRemove = new List<int>();
            for (var i = graph.Count - 1; i >= 0; i--)
            {
                var node = graph[i];
                if (node.neighbours.Any(n => graph[n].nearestEdgeDistance > node.nearestEdgeDistance))
                {
                    listToRemove.Add(i);
                }
            }

            for (var index = 0; index < listToRemove.Count; index++)
            {
                graph.RemoveAt(listToRemove[index]);
            }

            for (var i = 0; i < graph.Count; i++)
            {
                graph[i].index = i;
            }

            foreach (var node in graph)
            {
                List<int> newNeighbours = new List<int>();
                foreach (var neighbour in graph)
                {
                    var origin = node.position + raycastOffset;
                    var target = neighbour.position + raycastOffset;
                    bool result = Physics.Raycast(origin, (target - origin).normalized, 1000f);
                    if (!result)
                    {
                        newNeighbours.Add(neighbour.index);
                    }
                }
                node.neighbours = newNeighbours;
            }
        }

        public void SetWeight(Vector3 center, float setWeightRadius, int weight)
        {
            var nodes = graph.Where(node => Vector3.Distance(node.position, center) < setWeightRadius);
            foreach (var node in nodes)
            {
                node.weight = weight;
            }
        }

        private bool ValidPoint(Vector3 point)
        {
            Vector3 initialOffset = surfaceForward;
            const int numSamples = 16;
            float angleDiff = 360f / numSamples;
            for (float angle = 0; angle < 360f; angle+=angleDiff)
            {
                var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
                var raycastOrigin = point + 0.05f * surfaceNormal;
                bool result = Physics.Raycast(raycastOrigin, direction, radius);
                if (result)
                {
                    return false;
                }
            }
            
            return graph.All(visitedPoint =>
                Vector3.Distance(visitedPoint.position, point) > 0.5f * distanceBetweenPoints);
        }

        private float GetNearestEdgeDistance(Vector3 center)
        {
            Vector3 initialOffset = surfaceForward;
            float angleDiff = 360f / samplesPerPoint;
            float nearestEdgeDistance = float.MaxValue;
            for (float angle = 0; angle < 360; angle += angleDiff)
            {
                var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
                var raycastOrigin = center + 0.05f * surfaceNormal;
                bool result = Physics.Raycast(raycastOrigin, direction, out RaycastHit hitInfo);

                if (!result)
                {
                    continue;
                }

                nearestEdgeDistance = Mathf.Min(nearestEdgeDistance, Vector3.Distance(raycastOrigin, hitInfo.point));
            }

            return nearestEdgeDistance;
        }

        private List<Vector3> GetNeighbourPoints(Vector3 center)
        {
            Vector3 initialOffset = surfaceForward;
            float angleDiff = 360f / samplesPerPoint;

            var neighbours = new List<Vector3>();
            for (float angle = 0; angle < 360; angle += angleDiff)
            {
                var direction = (Quaternion.AngleAxis(angle, surfaceNormal) * initialOffset).normalized;
                bool result = Physics.Raycast(center + 0.05f * surfaceNormal, direction, distanceBetweenPoints);

                if (result)
                {
                    continue;
                }

                Vector3 offset = direction * distanceBetweenPoints;
                neighbours.Add(center + offset);
            }

            return neighbours;
        }
        

        [Serializable]
        public class Point
        {
            public Vector3 position;
            public int index;

            public Point(Vector3 position, int index)
            {
                this.position = position;
                this.index = index;
            }
        }
    }
}