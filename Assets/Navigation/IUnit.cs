using UnityEngine;

namespace com.ats.navigation
{
    public interface IUnit
    {
        Vector3 Velocity { get; set; }
        Vector3 Position { get; set; }
        int Index { get; set; }
        float Radius { get; }
        float MaxSpeed { get; }
    }
}