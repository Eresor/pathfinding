using System.Collections.Generic;
using UnityEngine;

namespace com.ats.navigation.Pooling
{
    public class Pool<T> where T : class, IPoolable, new()
    {
        private static List<T> pool = new List<T>();

        public static T Get()
        {
            for (int i = 0; i < pool.Count; ++i)
            {
                if (pool[i].IsPooled)
                {
                    continue;
                }

                pool[i].IsPooled = true;
                return pool[i];
            }

            var newInstance = new T();
            pool.Add(newInstance);
            newInstance.IsPooled = true;
            return newInstance;
        }
    }
}