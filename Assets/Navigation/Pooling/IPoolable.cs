using System;

namespace com.ats.navigation.Pooling
{
    public interface IPoolable : IDisposable
    {
        bool IsPooled { get; set; }
    }
}