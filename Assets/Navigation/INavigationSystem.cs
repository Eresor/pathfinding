using System;
using com.ats.navigation.surface;
using UnityEngine;

namespace com.ats.navigation
{
    public interface INavigationSystem : IDisposable
    {
        void AddUnit(IUnit unit);
        void RemoveUnit(IUnit unit);
        void Move(IUnit unit, Vector3 target);
        void SetSurface(ISurface surface);
    }

    public static class NavigationSystemExtensions
    {
        public static void MoveToUnit(this INavigationSystem navigationSystem, IUnit unitToMove, IUnit target)
        {
            var direction = (unitToMove.Position - target.Position).normalized;
            var desiredPosition = target.Position + direction * (unitToMove.Radius + target.Radius - 0.2f);
            navigationSystem.Move(unitToMove,desiredPosition);
        }
        
        public static void StopUnit(this INavigationSystem navigationSystem, IUnit unit)
        {
            navigationSystem.Move(unit,unit.Position);
        }
    }
}