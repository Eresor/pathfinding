using Unity.Mathematics;

namespace com.ats.navigation.DataOriented
{
    public struct NavigationTarget
    {
        public int index;
        public float3 position;
    }
}