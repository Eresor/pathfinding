﻿using Unity.Mathematics;
using UnityEngine;

namespace com.ats.navigation.DataOriented
{
    public class UnitComponent : MonoBehaviour, IUnit
    {
        public float radius = 1f;
        public Vector3 Velocity { get; set; }
        public Vector3 Position { get; set; }
        public int Index { get; set; }
        public float Radius => radius;
        public float MaxSpeed => 1f;
    }
}