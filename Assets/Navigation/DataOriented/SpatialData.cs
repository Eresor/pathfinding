using Unity.Mathematics;

namespace com.ats.navigation.DataOriented
{
    public struct SpatialData
    {
        public int index;
        public float3 position;
        public float3 velocity;
        public float radius;
        public float maxSpeed;
        
        public static SpatialData Empty => new SpatialData()
        {
            index = -1
        };
    }
}