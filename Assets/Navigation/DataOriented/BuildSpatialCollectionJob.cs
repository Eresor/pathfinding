﻿using System;
using System.Diagnostics.Contracts;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Unity.Mathematics;

namespace com.ats.navigation.DataOriented
{
    [BurstCompile]
    public struct BuildSpatialCollectionJob : IJob, IDisposable
    {
        [ReadOnly]
        private NativeList<SpatialData> _elements;
        private int _maxGroupSize;
        private int _maxTreeDepth;
        private SpatialCollection _spatialCollection;
        private int _nodeIndexCounter;

        public BuildSpatialCollectionJob(int maxGroupSize, NativeList<SpatialData> elements)
        {
            _nodeIndexCounter = 0;
            _elements = elements;
            _maxGroupSize = maxGroupSize;
            int numElements = elements.Length;
            int numGroups = Mathf.CeilToInt(numElements);
            _maxTreeDepth = Mathf.CeilToInt(Mathf.Log(numGroups, 4));
            _spatialCollection = new SpatialCollection(_maxTreeDepth,_maxGroupSize,Allocator.Persistent);
        }

        public void Execute()
        {
            Build(1,_elements,0);
        }

        public SpatialCollection Result => _spatialCollection;

        private void Build(int nodeIdx, NativeList<SpatialData> elements, int depth)
        {
            int numElements = elements.Length;

            if (numElements == 0)
            {
                _spatialCollection.SetNode(nodeIdx,new SpatialNode()
                {
                    center = float3.zero,
                    indexInElementsArray = -1,
                    numElements = -1,
                });
                return;
            }
            
            var node = new SpatialNode();

            float3 averagePos = float3.zero;
            for (var i = 0; i < numElements; i++)
            {
                averagePos += elements[i].position;
            }
            averagePos /= numElements;
            
            var _minXminY = new NativeList<SpatialData>(Allocator.Temp);
            var _maxXminY = new NativeList<SpatialData>(Allocator.Temp);
            var _maxXmaxY = new NativeList<SpatialData>(Allocator.Temp);
            var _minXmaxY = new NativeList<SpatialData>(Allocator.Temp);

            node.center = averagePos;

            if (numElements < _maxGroupSize || depth >= _maxTreeDepth)
            {
                node.numElements = numElements;
                node.indexInElementsArray = _nodeIndexCounter;
                ++_nodeIndexCounter;
                for (int i = 0; i < numElements; ++i)
                {
                    _spatialCollection.SetSpatialDataAtIndex(node.indexInElementsArray,i,elements[i]);
                }
                _spatialCollection.SetNode(nodeIdx,node);
                
                _minXminY.Dispose();
                _maxXminY.Dispose();
                _maxXmaxY.Dispose();
                _minXmaxY.Dispose();
                return;
            }

            _spatialCollection.SetNode(nodeIdx,node);
            if (numElements == 0)
            {
                _minXminY.Dispose();
                _maxXminY.Dispose();
                _maxXmaxY.Dispose();
                _minXmaxY.Dispose();
                return;
            }

            for (int i = 0; i < numElements; ++i)
            {
                var element = elements[i];
                float3 position = elements[i].position;
                
                if (position.x <= averagePos.x && position.z <= averagePos.z)
                {
                    _minXminY.Add(element);
                }
                else if (position.x > averagePos.x && position.z <= averagePos.z)
                {
                    _maxXminY.Add(element);
                }
                else if (position.x > averagePos.x && position.z > averagePos.z)
                {
                    _maxXmaxY.Add(element);
                }
                else
                {
                    _minXmaxY.Add(element);
                }
            }

            Build(_spatialCollection.GetChildIndex(nodeIdx,0), _minXminY, depth + 1);//if (_minXminY.Length > 0) 
            Build(_spatialCollection.GetChildIndex(nodeIdx,1), _maxXminY, depth + 1);//if (_maxXminY.Length > 0) 
            Build(_spatialCollection.GetChildIndex(nodeIdx,2), _maxXmaxY, depth + 1);//if (_maxXmaxY.Length > 0) 
            Build(_spatialCollection.GetChildIndex(nodeIdx,3), _minXmaxY, depth + 1);//if (_minXmaxY.Length > 0) 
            
            _minXminY.Dispose();
            _maxXminY.Dispose();
            _maxXmaxY.Dispose();
            _minXmaxY.Dispose();
        }
        

        public void Dispose()
        {
            _spatialCollection.Dispose();
        }
    }

    public struct SpatialCollection : IDisposable
    {
        private NativeMatrix<SpatialData> _elements;
        
        private NativeArray<SpatialNode> _nodes;

        public SpatialCollection(int depth, int maxGroupSize, Allocator allocator)
        {
            _elements = new NativeMatrix<SpatialData>(maxGroupSize,(int)math.pow(4,depth),allocator);
            int requiredCapacity = 1;
            for (int i = 0; i <= depth+1; ++i)
            {
                requiredCapacity += (int)math.pow(4,i);
            }
            _nodes = new NativeArray<SpatialNode>(requiredCapacity, allocator);
        }
        
        public SpatialNode GetRoot()
        {
            return _nodes[1]; //start with 1
        }

        public void SetNode(int index, SpatialNode node)
        {
            _nodes[index] = node;
        }
        
        public SpatialNode GetNode(int index)
        {
            return _nodes[index];
        }

        [Pure]
        public NativeSlice<SpatialData> GetNeighbours(float3 position)
        {
            var currentNodeIdx = 1;
            SpatialNode currentNode = _nodes[currentNodeIdx];
            while (currentNode.numElements == 0)
            {
                var nearestChildIdx = -1;
                var minDistance = float.MaxValue;
                for (int i = 0; i < 4; ++i)
                {
                    var childIndex = GetChildIndex(currentNodeIdx, i);
                    var childNode = _nodes[childIndex];
                    if (childNode.numElements == -1)
                    {
                        continue;
                    }
                    var childPosition = childNode.center;
                    var distance = math.distancesq(childPosition, position);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        nearestChildIdx = childIndex;
                    }
                }
                currentNodeIdx = nearestChildIdx;
                currentNode = _nodes[currentNodeIdx];
            }

            return _elements.GetRow(currentNode.indexInElementsArray, currentNode.numElements);
        }

        public void SetSpatialDataAtIndex(int nodeIndex, int elementIndex, SpatialData data)
        {
            _elements[nodeIndex, elementIndex] = data;
        }

        public int GetChildIndex(int parentIndex, int child)
        {
            return parentIndex * 4 - 2 + child;
        }

        private int GetParentIndex(int childIndex)
        {
            return (childIndex + 2) / 4;
        }

        public void Dispose()
        {
            _nodes.Dispose();
            _elements.Dispose();
        }
    }
}