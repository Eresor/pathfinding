﻿using com.ats.navigation.surface.Data;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace com.ats.navigation.DataOriented
{
    [BurstCompile]
    public struct CalculateUnitVelocityJob : IJobParallelFor
    {
        private NativeArray<SpatialData> _spatialData;
        [ReadOnly] private NativeList<NavigationTarget> _targets;
        [ReadOnly] private readonly NativeArray<AvoidanceQueryResult> _avoidanceQueryResults;
        [ReadOnly] private readonly GridSurfaceNativeData _surfaceData;
        [ReadOnly] private readonly float _maxAngleDeltaRadians;

        public CalculateUnitVelocityJob(GridSurfaceNativeData surfaceData, NativeList<NavigationTarget> targets, float maxAngleDeltaRadians, NativeArray<AvoidanceQueryResult> avoidanceQueryResults,
            NativeArray<SpatialData> spatialData)
        {
            _avoidanceQueryResults = avoidanceQueryResults;
            _spatialData = spatialData;
            _surfaceData = surfaceData;
            _targets = targets;
            _maxAngleDeltaRadians = maxAngleDeltaRadians;
        }

        public void Execute(int index)
        {
            var unit = _spatialData[index];
            var avoidanceData = _avoidanceQueryResults[index];
            var navigationVelocity = GetDirectionFromNavigation(index);

            float3 desiredVelocity = avoidanceData.willCollide && avoidanceData.nearestNeighbourIdx > index
                ? GetDirectionFromCollisionAvoidance(index)
                : navigationVelocity;


            var angle = Vector3.Angle(unit.velocity, desiredVelocity);
            var desiredVelocityVector = angle > 1f ? Vector3.Slerp(unit.velocity, desiredVelocity,
                _maxAngleDeltaRadians * (1f - angle/360f)) : new Vector3(desiredVelocity.x,desiredVelocity.y,desiredVelocity.z);
            
            unit.velocity = new Vector3(desiredVelocityVector.x,desiredVelocityVector.y,desiredVelocityVector.z).normalized * unit.maxSpeed;
            unit.position += unit.velocity;
            _spatialData[index] = unit;
        }

        private float3 GetDirectionFromNavigation(int index)
        {
            var unit = _spatialData[index];
            var target = _targets[index];
            var position = unit.position;
            Vector3 positionVector = unit.position;

            if (target.index == -1)
            {
                return float3.zero;
            }

            if (math.distancesq(position, target.position) <= unit.radius * unit.radius)
            {
                return float3.zero;
            }

            var currentNodeIndex = _surfaceData.GetNearestNode(position).index;
            var findPathJob = new GridPathFinderJob(currentNodeIndex, target.index, new NativeSlice<NativeNode>(_surfaceData.graph), _surfaceData.visibility.AsSlice());
            findPathJob.Execute();
            var path = findPathJob.path;
            var pathLength = path.Length;
            //todo: fix this condition for a*
            if (pathLength == 0 || pathLength == 0) //<=1 for a* 
            {
                findPathJob.Dispose();
                return math.normalize(target.position - position);
            }

            var nextNodePosition = _surfaceData.graph[path[0]].position;
            findPathJob.Dispose();
            return math.normalize(nextNodePosition - position);
        }

        private float3 GetDirectionFromCollisionAvoidance(int index)
        {
            var unit = _spatialData[index];
            var avoidanceData = _avoidanceQueryResults[index];

            var nearestNode = _surfaceData.GetNearestNode(unit.position);
            var avoidancePosition = unit.position +
                                    unit.maxSpeed * math.normalize(avoidanceData.collisionAvoidanceDirection);

            //n0
            float nearestNeighbourDistance =
                math.distancesq(avoidancePosition, _surfaceData.graph[nearestNode.neighbour0].position);

            var nearestNeighbour = _surfaceData.graph[nearestNode.neighbour0];

            //n1
            var distance = math.distancesq(avoidancePosition, _surfaceData.graph[nearestNode.neighbour1].position);
            if (distance < nearestNeighbourDistance)
            {
                nearestNeighbourDistance = distance;
                nearestNeighbour = _surfaceData.graph[nearestNode.neighbour1];
            }

            //n2
            distance = math.distancesq(avoidancePosition, _surfaceData.graph[nearestNode.neighbour2].position);
            if (distance < nearestNeighbourDistance)
            {
                nearestNeighbourDistance = distance;
                nearestNeighbour = _surfaceData.graph[nearestNode.neighbour2];
            }

            //n3
            distance = math.distancesq(avoidancePosition, _surfaceData.graph[nearestNode.neighbour3].position);
            if (distance < nearestNeighbourDistance)
            {
                nearestNeighbourDistance = distance;
                nearestNeighbour = _surfaceData.graph[nearestNode.neighbour3];
            }

            //n4
            distance = math.distancesq(avoidancePosition, _surfaceData.graph[nearestNode.neighbour4].position);
            if (distance < nearestNeighbourDistance)
            {
                nearestNeighbourDistance = distance;
                nearestNeighbour = _surfaceData.graph[nearestNode.neighbour4];
            }
            
            //n5
            distance = math.distancesq(avoidancePosition, _surfaceData.graph[nearestNode.neighbour5].position);
            if (distance < nearestNeighbourDistance)
            {
                nearestNeighbourDistance = distance;
                nearestNeighbour = _surfaceData.graph[nearestNode.neighbour5];
            }

            return math.normalize(nearestNeighbour.position - unit.position);
        }
    }
}