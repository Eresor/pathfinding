﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using Unity.Collections;

namespace com.ats.navigation.DataOriented
{
    public struct NativeMatrix<T> : IDisposable where T : struct
    {
        public readonly int width;
        public readonly int height;
        private NativeArray<T> _array;

        public NativeMatrix(int width, int height,Allocator allocator)
        {
            this.width = width;
            this.height = height;
            _array = new NativeArray<T>(width*height,allocator);
        }

        public T this[int y, int x]
        {
            get => _array[y * width + x];
            set
            {
                if (y * width + x >= _array.Length)
                {
                    int a;
                }
                _array[y * width + x] = value;
            }
        }
        
        public NativeSlice<T> GetRow(int row, int length = -1)
        {
            if (length == -1)
            {
                length = width;
            }
            return new NativeSlice<T>(_array,row*width,length);
        }
        
        [Pure]
        public Slice AsSlice() => new Slice(width,height,_array);

        public void Dispose()
        {
            if (_array.IsCreated)
            {
                _array.Dispose();
            }
        }
        
        public struct Slice
        {
            public readonly int width;
            public readonly int height;
            private NativeSlice<T> _array;

            public Slice(int width, int height, NativeArray<T> array)
            {
                this.width = width;
                this.height = height;
                _array = new NativeSlice<T>(array);
            }

            public T this[int y, int x]
            {
                get => _array[y * width + x];
                set => _array[y * width + x] = value;
            }
        
            public NativeSlice<T> GetRow(int row, int length = -1)
            {
                if (length == -1)
                {
                    length = width;
                }
                return new NativeSlice<T>(_array,row*width,length);
            }
        }
    }
}