﻿using System;
using com.ats.navigation.surface.Data;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Unity.Mathematics;

namespace com.ats.navigation.DataOriented
{
    [BurstCompile]
    public struct GridPathFinderJob : IJob, IDisposable
    {
//        private readonly Func<int, int> _heuristic;
//        private readonly Func<int, int, int> _distance;
//        private readonly Func<int, List<int>> _neighbours;

        private int _startIdx;
        private int _endIdx;
        [ReadOnly]
        private NativeSlice<NativeNode> _graph;
        private NativeMatrix<int>.Slice _visibility;
        
        private int _numNodes;
        
        private NativeHashMap<int, int> closedSet;
        private NativeHashMap<int, int> hScore;
        private NativeHashMap<int, int> gScore;
        private NativeHashMap<int, int> cameFromIdx;
        public NativeList<int> invertedPath;
        public NativeList<int> path;

        public GridPathFinderJob(int startIdx, int endIdx, NativeSlice<NativeNode> graph, NativeMatrix<int>.Slice visibility)
        {
            _startIdx = startIdx;
            _endIdx = endIdx;
            _graph = graph;
            _visibility = visibility;
            _numNodes = _graph.Length;
            _indexedNodes = new NativeHashMap<int, int>(_numNodes,Allocator.Temp);
            _sortedNodes = new NativeList<Node>(Allocator.Temp);
            closedSet = new NativeHashMap<int, int>(_numNodes,Allocator.Temp);
            hScore = new NativeHashMap<int, int>(_numNodes,Allocator.Temp);
            gScore = new NativeHashMap<int, int>(_numNodes,Allocator.Temp);
            cameFromIdx = new NativeHashMap<int, int>(_numNodes,Allocator.Temp);
            invertedPath = new NativeList<int>(Allocator.Temp);
            path = new NativeList<int>(Allocator.Temp);
        }

        public void FindPath()
        {
            Add(_startIdx, 0);
            gScore[_startIdx] = 0;
            cameFromIdx[_startIdx] = _startIdx;

            while (Empty() == false)
            {
                var currentIdx = Min();
                if (currentIdx == _endIdx)
                {
                    currentIdx = _endIdx;
                    while (currentIdx != _startIdx)
                    {
                        invertedPath.Add(currentIdx);
                        currentIdx = cameFromIdx[currentIdx];
                    }

                    //todo: better invert path
                    for (var i = invertedPath.Length - 1; i >= 0; i--)
                    {
                        path.Add(invertedPath[i]);
                    }

                    return;
                }

                Remove(currentIdx);
                closedSet.Add(currentIdx, 0);

                var list = new NativeList<int>(Allocator.Temp);
                GetNeighbours(list,currentIdx);
                for (var index = 0; index < list.Length; index++)
                {
                    var neighbourIdx = list[index];
                    if (closedSet.ContainsKey(neighbourIdx))
                    {
                        continue;
                    }

                    if (!Contains(neighbourIdx))
                    {
                        gScore[neighbourIdx] = int.MaxValue;
                        cameFromIdx[neighbourIdx] = -1;
                    }
                    
                    var currentParentIdx = cameFromIdx[currentIdx];
                    if(GetIsLineOfSigh(currentParentIdx,neighbourIdx))
                    {
                        var gScoreParent = gScore[currentParentIdx];
                        var distanceParent = Distance(currentParentIdx, neighbourIdx);
                        if (gScoreParent + distanceParent < gScore[neighbourIdx])
                        {
                            gScore[neighbourIdx] = gScoreParent + distanceParent;
                            cameFromIdx[neighbourIdx] = currentParentIdx;
                            if (Contains(neighbourIdx))
                            {
                                Remove(neighbourIdx);
                            }
                            Add(neighbourIdx, gScore[neighbourIdx] + Heuristic(neighbourIdx));
                        }
                    }
                    else
                    {
                        var currentScore = gScore[currentIdx];
                        var currentDistance = Distance(currentIdx, neighbourIdx);
                        if (currentScore + currentDistance < gScore[neighbourIdx])
                        {
                            gScore[neighbourIdx] = currentScore + currentDistance;
                            cameFromIdx[neighbourIdx] = currentIdx;
                            if (Contains(neighbourIdx))
                            {
                                Remove(neighbourIdx);
                            }
                            Add(neighbourIdx, gScore[neighbourIdx] + Heuristic(neighbourIdx));
                        }
                    }
                }
                list.Dispose();
            }
        }

        private bool GetIsLineOfSigh(int from, int to)
        {
            return _visibility[from,to] == 1;
        }

        private void GetNeighbours(NativeList<int> listToFill, int nodeIdx)
        {
            _graph[nodeIdx].GetNeighbours(listToFill);
        }

        private int Distance(int from, int to)
        {
            return (int)math.distance(_graph[from].position, _graph[to].position);
        }

        private int Heuristic(int nodeIdx)
        {
            return Distance(nodeIdx, _endIdx);
        }

        public void Execute()
        {
            FindPath();
        }
//    }

//    struct NodeCollection
//    {
        //todo refactor to separated class
        NativeHashMap<int, int> _indexedNodes;
        NativeList<Node> _sortedNodes;

        //todo refactor to separated class
        public bool Add(int idx, int score)
        {
            bool alreadyExist = _indexedNodes.ContainsKey(idx);

            if (!alreadyExist)
            {
                _sortedNodes.Add(new Node()
                {
                    Idx = idx,
                    Score = score
                });
                _indexedNodes[idx] = score;
                return true;
            }

            return false;
        }

        //todo refactor to separated class
        public int Min()
        {
            int min = int.MaxValue;
            int minIdx = 0;
            for (int i = 0; i < _sortedNodes.Length; ++i)
            {
                if (min > _sortedNodes[i].Score)
                {
                    min = _sortedNodes[i].Score;
                    minIdx = i;
                }
            }
            return _sortedNodes[minIdx].Idx;
        }
        
        //todo refactor to separated class
        public void Remove(int idx)
        {
            bool alreadyExist = _indexedNodes.ContainsKey(idx);
            if (alreadyExist)
            {
                _indexedNodes.Remove(idx);
                int numNodes = _sortedNodes.Length;
                for (int i = numNodes - 1; i >= 0; --i)
                {
                    if (_sortedNodes[i].Idx == idx)
                    {
                        _sortedNodes.RemoveAtSwapBack(i);
                    }
                }
            }
        }

        //todo refactor to separated class
        public void Clear()
        {
            _sortedNodes.Clear();
            _indexedNodes.Clear();
        }

        //todo refactor to separated class
        public bool Empty()
        {
            return _sortedNodes.Length == 0;
        }

        //todo refactor to separated class
        public bool Contains(int idx)
        {
            return _indexedNodes.ContainsKey(idx);
        }

        private struct Node : IComparable<Node>
        {
            public int Idx;
            public int Score;

            public int CompareTo(Node other)
            {
                int scoreResult = Score.CompareTo(other.Score);
                return scoreResult == 0 ? Idx.CompareTo(other.Idx) : scoreResult;
            }
        }

//    }
        public void Dispose()
        {
            invertedPath.Dispose();
            closedSet.Dispose();
            hScore.Dispose();
            gScore.Dispose();
            cameFromIdx.Dispose();
            _indexedNodes.Dispose();
            _sortedNodes.Dispose();
            path.Dispose();
        }
    }
}