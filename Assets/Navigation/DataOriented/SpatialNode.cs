using Unity.Mathematics;

namespace com.ats.navigation.DataOriented
{
    public struct SpatialNode
    {
        public float3 center;
        public int indexInElementsArray;
        public int numElements;
    }
}