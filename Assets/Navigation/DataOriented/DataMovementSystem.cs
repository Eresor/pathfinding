using System;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Jobs;

namespace com.ats.navigation.DataOriented
{
    public class DataMovementSystem : IDisposable
    {
        private List<NavigationModel> _navigationModels = new List<NavigationModel>();
        private NativeList<SpatialData> _unitsData;
        private TransformAccessArray _unitsTransformAccess;
        private int _numUnits = 0;

        public DataMovementSystem()
        {
            const int capacity = 1000;
            _unitsTransformAccess = new TransformAccessArray(capacity);
            _unitsData = new NativeList<SpatialData>(capacity,Allocator.Persistent);
        }

        public void Update()
        {
            for (var i = 0; i < _navigationModels.Count; i++)
            {
                var navigationModel = _navigationModels[i];
                var data = _unitsData[i];
                data.position = navigationModel.Position;
                data.velocity = navigationModel.Velocity;
                _unitsData[i] = data;
            }

            var moveUnitJob = new DataMovementSystemJob(_unitsData);
            var moveUnitJobHandle = moveUnitJob.Schedule(_unitsTransformAccess);
            moveUnitJobHandle.Complete();
        }

        public void Dispose()
        {
            _unitsTransformAccess.Dispose();
            _unitsData.Dispose();
        }

        public void AddUnit(NavigationModel navigationModel, Transform unit)
        {
            if (_unitsTransformAccess.length > _numUnits)
            {
                _unitsTransformAccess[_numUnits] = unit;
            }
            else
            {
                _unitsTransformAccess.Add(unit);
            }
            
            var unitData = new SpatialData()
            {
                index = navigationModel.Index,
                position = navigationModel.Position,
                radius = navigationModel.Radius,
                velocity = Vector3.zero
            };
            if (_unitsData.Length > _numUnits)
            {
                _unitsData[_numUnits] = unitData;
            }
            else
            {
                _unitsData.Add(unitData);
            }
            
            if (_navigationModels.Count > _numUnits)
            {
                _navigationModels[_numUnits] = navigationModel;
            }
            else
            {
                _navigationModels.Add(navigationModel);
            }

            _numUnits++;
        }

        public void RemoveUnit(Transform unit)
        {
            var unitIdxInArray = GetInternalIndex(unit);
            _unitsTransformAccess[unitIdxInArray] = _unitsTransformAccess[_numUnits - 1];
            _unitsData[unitIdxInArray] = _unitsData[_numUnits - 1];
        }
        
        private int GetInternalIndex(Transform unit)
        {
            for (int i = 0; i < _numUnits; ++i)
            {
                if (unit != _unitsTransformAccess[i])
                {
                    continue;
                }
                return i;
            }
            return -1;
        }
    }
}