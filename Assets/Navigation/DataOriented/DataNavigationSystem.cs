using System;
using System.Collections.Generic;
using com.ats.navigation.surface;
using com.ats.navigation.surface.Data;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace com.ats.navigation.DataOriented
{
    public class DataNavigationSystem : INavigationSystem
    {
        private IDisposable _moveCommand;
        private GridSurfaceNativeData _gridSurfaceNativeData;
        private NativeList<SpatialData> _unitsData;
        private NativeList<NavigationTarget> _targets;
        private List<IUnit> _units;
        private int _numUnits;

        public NativeList<SpatialData> UnitsData => _unitsData;
        
        public void Initialize()
        {
            const int capacity = 1000;
            _unitsData = new NativeList<SpatialData>(capacity, Allocator.Persistent);
            _targets = new NativeList<NavigationTarget>(capacity, Allocator.Persistent);
            _units = new List<IUnit>(capacity);
        }

        public void Dispose()
        {
            _gridSurfaceNativeData.Dispose();
            _unitsData.Dispose();
            _targets.Dispose();
        }

        public void Update()
        {
            for (var i = 0; i < _numUnits; i++)
            {
                var currentData = _unitsData[i];
                currentData.position = _units[i].Position;
                currentData.velocity = _units[i].Velocity;
                currentData.maxSpeed = _units[i].MaxSpeed * Time.deltaTime;
                _unitsData[i] = currentData;
            }
            
            var buildSpatialCollectionJob = new BuildSpatialCollectionJob(4, _unitsData);
            var handle = buildSpatialCollectionJob.Schedule();
            handle.Complete();
            var spatialCollection = buildSpatialCollectionJob.Result;

            var findAvoidanceVectorJob = new FindAvoidanceVectorJob(_unitsData, spatialCollection, Time.deltaTime);
            var findAvoidanceVectorJobHandle = findAvoidanceVectorJob.Schedule(_unitsData.Length, 32);
            findAvoidanceVectorJobHandle.Complete();
            // //todo: add max speed and rotation parameters to units
            var calculateVelocityJob = new CalculateUnitVelocityJob(_gridSurfaceNativeData, _targets,
                Mathf.Deg2Rad * 360 * Time.deltaTime, findAvoidanceVectorJob.Results,
                _unitsData);
            var calculateVelocityJobHandle = calculateVelocityJob.Schedule(_unitsData.Length, 32);
            calculateVelocityJobHandle.Complete();

            buildSpatialCollectionJob.Dispose();
            findAvoidanceVectorJob.Dispose();

            for (var i = 0; i < _numUnits; i++)
            {
                var unit = _units[i];
                var unitData = _unitsData[i];
                unit.Position = unitData.position;
                unit.Velocity = unitData.velocity;
            }
        }
        
        public void AddUnit(IUnit unit)
        {
            var externalIndex = GetNewExternalIndex();

            unit.Index = externalIndex;
            var newData = new SpatialData
            {
                index = externalIndex,
                radius = unit.Radius,
                velocity = float3.zero,
                position = unit.Position
            };

            var defaultTarget = new NavigationTarget()
            {
                index = -1,
                position = unit.Position
            };

            if (_unitsData.Length > _numUnits)
            {
                _unitsData[_numUnits] = newData;
            }
            else
            {
                _unitsData.Add(newData);
            }

            if (_targets.Length > _numUnits)
            {
                _targets[_numUnits] = defaultTarget;
            }
            else
            {
                _targets.Add(defaultTarget);
            }
            
            if (_units.Count > _numUnits)
            {
                _units[_numUnits] = unit;
            }
            else
            {
                _units.Add(unit);
            }

            _numUnits++;
        }

        public void RemoveUnit(IUnit unit)
        {
            var unitIdxInArray = GetInternalArrayIndex(unit);
            if (unitIdxInArray < 0)
            {
                return;
            }
            _unitsData[unitIdxInArray] = _unitsData[_numUnits - 1];
            _targets[unitIdxInArray] = _targets[_numUnits - 1];
            _units[unitIdxInArray] = _units[_numUnits - 1];
            _unitsData[_numUnits-1] = SpatialData.Empty;
            _units[_numUnits - 1] = null;
            --_numUnits;
        }

        public void Move(IUnit unit, Vector3 target)
        {
            var index = GetInternalArrayIndex(unit);
            _targets[index] = new NavigationTarget()
            {
                index = _gridSurfaceNativeData.GetNearestNode(target).index,
                position = target
            };
        }

        public void SetSurface(ISurface surface)
        {
            _gridSurfaceNativeData.Dispose();
            _gridSurfaceNativeData = surface.GetNativeData(Allocator.Persistent);
        }

        private int GetInternalArrayIndex(IUnit unit)
        {
            return GetInternalArrayIndex(unit.Index);
        }

        private int GetInternalArrayIndex(int unitIndex)
        {
            for (int i = 0; i < _numUnits; ++i)
            {
                if (_unitsData[i].index != unitIndex)
                {
                    continue;
                }

                return i;
            }

            return -1;
        }

        private static int _externalIndexCounter;

        private static int GetNewExternalIndex()
        {
            return _externalIndexCounter++;
        }
    }
}