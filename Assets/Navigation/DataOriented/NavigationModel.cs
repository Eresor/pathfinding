using System;
using UnityEngine;

namespace com.ats.navigation.DataOriented
{
    [Serializable]
    public class NavigationModel : IUnit
    {
        [SerializeField] private Vector3 position;

        public NavigationModel()
        {
            MaxSpeed = 1f;
        }
        
        public NavigationModel(float maxSpeed)
        {
            MaxSpeed = maxSpeed;
        }

        public Vector3 Velocity { get; set; }

        public Vector3 Position
        {
            get => position;
            set => position = value;
        }

        public int Index { get; set; }

        public float Radius { get; set; }
        public float MaxSpeed { get; }
    }
}