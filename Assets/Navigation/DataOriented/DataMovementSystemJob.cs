using Unity.Burst;
using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace com.ats.navigation.DataOriented
{
    [BurstCompile]
    public struct DataMovementSystemJob : IJobParallelForTransform
    {
        [ReadOnly] private readonly NativeList<SpatialData> _spatialData;

        public DataMovementSystemJob(NativeList<SpatialData> spatialData)
        {
            _spatialData = spatialData;
        }
        
        public void Execute(int index, TransformAccess transform)
        {
            var data = _spatialData[index];
            transform.position = data.position;
            if (math.length(data.velocity) > 0)
            {
                transform.rotation = Quaternion.LookRotation(math.normalize(data.velocity),Vector3.up);
            }
        }
    }
}