using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace com.ats.navigation.DataOriented
{
    [BurstCompile]
    public struct FindAvoidanceVectorJob : IJobParallelFor, IDisposable
    {
        [ReadOnly] private readonly NativeList<SpatialData> _units;
        [ReadOnly] private readonly SpatialCollection _spatialCollection;
        [WriteOnly] public NativeArray<AvoidanceQueryResult> Results;
        private float _timestep;

        public FindAvoidanceVectorJob(NativeList<SpatialData> units, SpatialCollection spatialCollection, float timestep)
        {
            _units = units;
            _spatialCollection = spatialCollection;
            _timestep = timestep;
            Results = new NativeArray<AvoidanceQueryResult>(units.Length,Allocator.TempJob);
        }
        
        public void Execute(int index)
        {
            var unit = _units[index];
            NativeSlice<SpatialData> neighbours = _spatialCollection.GetNeighbours(unit.position);
            var collisionData = CheckCollision(unit,neighbours);
            var result = new AvoidanceQueryResult {willCollide = collisionData.result};
            if (collisionData.result)
            {
                var avoidanceData = GetAvoidanceVector(unit, neighbours,_timestep);
                result.collisionAvoidanceDirection = avoidanceData.vector;
                result.nearestNeighbourDistance = avoidanceData.distance;
                result.nearestNeighbourIdx = avoidanceData.nearestNeighbourIdx;
            }
            Results[index] = result;
        }
        
        
        private AvoidanceData GetAvoidanceVector(SpatialData unit, NativeSlice<SpatialData> neighbours, float timestep)
        {
            var predictedPositions = new NativeList<float3>(Allocator.Temp);
            var nearestDistance = float.MaxValue;
            var nearestIdx = -1;
            for (var i = 0; i < neighbours.Length; i++)
            {
                var boid = neighbours[i];
                if (boid.index == unit.index)
                {
                    continue;
                }

                var boidPredictedPos = boid.position + boid.velocity * timestep;
                predictedPositions.Add(boidPredictedPos);
                var distance = math.distance(boidPredictedPos, unit.position) - boid.radius;
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestIdx = boid.index;
                }
            }
            
            float3 avereage = float3.zero;
            for (var i = 0; i < predictedPositions.Length; i++)
            {
                var predictedPosition = predictedPositions[i];
                var directionToPredicted = math.normalize((predictedPosition - unit.position));
                avereage += directionToPredicted;
            }
            avereage /= predictedPositions.Length;
            
            predictedPositions.Dispose();
            return new AvoidanceData()
            {
                distance = nearestDistance,
                nearestNeighbourIdx = nearestIdx,
                vector = (-avereage)*math.length(unit.velocity)
            };
        }


        private CollisionQueryData CheckCollision(SpatialData boid, NativeSlice<SpatialData> neighbours)
        {
            var timestep = boid.radius / math.length(boid.velocity);
            var collisionQueryData = new CollisionQueryData()
            {
                distance = float.MaxValue,
                result = false
            };
            for (var i = 0; i < neighbours.Length; i++)
            {
                var neighbour = neighbours[i];
                if (neighbour.index == boid.index)
                {
                    continue;
                }
                var predictedNeighbourPosition = neighbour.position + timestep * neighbour.velocity;
                var distance = math.distance(boid.position + boid.velocity * timestep, predictedNeighbourPosition);
                collisionQueryData.distance = math.min(collisionQueryData.distance, distance);
                if(distance < neighbour.radius)
                {
                    collisionQueryData.result = true;
                    break;
                }
            }

            return collisionQueryData;
        }
        
        private struct CollisionQueryData
        {
            public bool result;
            public float distance;
        }

        private struct AvoidanceData
        {
            public float3 vector;
            public float distance;
            public float nearestNeighbourIdx;
        }

        public void Dispose()
        {
            Results.Dispose();
        }
    }

    public struct AvoidanceQueryResult
    {
        public bool willCollide;
        public float nearestNeighbourDistance;
        public float nearestNeighbourIdx;
        public float3 collisionAvoidanceDirection;
    }
}