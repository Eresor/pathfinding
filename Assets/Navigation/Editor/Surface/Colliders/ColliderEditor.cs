﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Collider = com.ats.surface.colliders.Collider;
namespace com.ats.navigation.editor.surface.colliders
{
        [CustomEditor(typeof(Collider), true)]
        public class ColliderEditor : Editor
        {
            private readonly List<int> _lineIndicies = new List<int>();
            private Material _material;
            private Mesh _editorMesh;
    
            private void OnSceneGUI()
            {
                var collider = target as Collider;
                if (collider == null)
                {
                    return;
                }
    
                var mesh = collider.mesh;
    
                if (mesh?.vertices?.Count == 0 || mesh?.triangles?.Count == 0)
                {
                    return;
                }
    
                if (_editorMesh == null)
                {
                    _editorMesh = new Mesh();
                    _editorMesh.vertices = mesh.vertices.ToArray();
                    _editorMesh.triangles = mesh.triangles.ToArray();
                }
    
                if (_material == null)
                {
                    _material = new Material(Shader.Find("Unlit/Wireframe"));
                }
    
                Graphics.DrawMesh(_editorMesh, collider.transform.localToWorldMatrix, _material, 0,
                    SceneView.currentDrawingSceneView.camera);
            }
    
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                if (GUILayout.Button("Rebuild mesh"))
                {
                    (target as Collider)?.RebuildMesh();
                    _editorMesh = null;
                }
            }
        }

}