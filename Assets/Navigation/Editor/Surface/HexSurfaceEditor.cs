using com.ats.navigation.surface;
using UnityEditor;

namespace com.ats.navigation.editor.surface
{
    [CustomEditor(typeof(HexSurface))]
    public class HexSurfaceEditor : SurfaceEditor
    {
    }
}