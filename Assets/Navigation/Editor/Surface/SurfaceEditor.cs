﻿using com.ats.navigation.surface;
using UnityEngine;
using UnityEditor;

namespace com.ats.navigation.editor.surface
{
    public class SurfaceEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Generate"))
            {
                (target as ISurfaceEditor).Generate();
            }

            base.OnInspectorGUI();
        }
        
        protected void OnSceneGUI()
        {
            var surfaceComponent = target as ISurfaceEditor;

            var textStyle = new GUIStyle();
            textStyle.normal.textColor = Color.black;
            Vector3[] line = new Vector3[2];
            int numNodes = surfaceComponent.Graph.Count;
            for (var i = 0; i < numNodes; i++)
            {
                var node = surfaceComponent.Graph[i];
                line[0] = node.position;

                Handles.Label(node.position + 0.2f * surfaceComponent.SurfaceNormal, node.index.ToString(), textStyle);
                // if (node is HexSurface.HexNode hexNode)
                // {
                //     Handles.Label(node.position + 0.2f * surfaceComponent.SurfaceNormal, hexNode.gridIndex.ToString(), textStyle);
                // }
                // else
                // {
                //     Handles.Label(node.position + 0.2f * surfaceComponent.SurfaceNormal, node.index.ToString(), textStyle);
                // }
                
                foreach (var neighbour in node.neighbours)
                {
                    line[1] = surfaceComponent.Graph[neighbour].position;
                    Handles.DrawAAPolyLine(3,line);
                }
            }

            if (surfaceComponent.VisibilityGraph == null || !surfaceComponent.VisibilityGraphInitialized)
            {
                return;
            }

            if (!surfaceComponent.DrawVisibilityGraphEditor)
            {
                return;                
            }
            Handles.color = Color.red;
            for (var i = 0; i < numNodes; i++)
            {
                for (int j = 0; j < numNodes; j++)
                {
                    if (false == surfaceComponent.VisibilityGraph.Get(i, j))
                    {
                        continue;
                    }

                    line[0] = surfaceComponent.Graph[i].position;
                    line[1] = surfaceComponent.Graph[j].position;
                    Handles.DrawPolyLine(line);
                }
            }
        }
    }
}